﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class SongXMLManager
{
    private XmlDocument xmlDocument;
    private TextAsset textXML;

    public List<Secuence> secuencesBySong;

    public void LoadXML()
    {
        textXML = Resources.Load("XML/Songs") as TextAsset;
        xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(textXML.ToString());
    }
    public void LoadSong(int songValue)
    {
        Debug.Log(xmlDocument.DocumentElement.SelectSingleNode("/Songs/Song" + songValue + "/NumberSecuences").InnerText);
        int numberSecuences = int.Parse(xmlDocument.DocumentElement.SelectSingleNode("/Songs/Song" + songValue + "/NumberSecuences").InnerText);

        secuencesBySong = new List<Secuence>();
        secuencesBySong.Clear();
       
        for (int i = 1; i <= numberSecuences; i++)
        {
            Secuence secuence = new Secuence();
            secuence.time = float.Parse(xmlDocument.DocumentElement.SelectSingleNode("/Songs/Song" + songValue + "/Secuence" + i + "/Time").InnerText);
            secuence.action = xmlDocument.DocumentElement.SelectSingleNode("/Songs/Song" + songValue + "/Secuence" + i + "/Action").InnerText;
            secuencesBySong.Add(secuence);
        }
    }

    public void ChangeValue(int songValue, int secuenceValue, float timeValue)
    {
        //xmlDocument.DocumentElement.SetAttribute("/Songs/Song" + songValue + "/Secuence" + secuenceValue + "/Time", timeValue.ToString());
        Debug.Log(xmlDocument.DocumentElement.SelectSingleNode("/Songs/Song" + songValue + "/Secuence" + secuenceValue + "/Time").InnerText);
        xmlDocument.DocumentElement.SelectSingleNode("/Songs/Song" + songValue + "/Secuence" + secuenceValue + "/Time").InnerText = timeValue.ToString();
        Debug.Log(xmlDocument.DocumentElement.SelectSingleNode("/Songs/Song" + songValue + "/Secuence" + secuenceValue + "/Time").InnerText);
        
    }

    public void CreateSecuence()
    {
        Debug.Log("CreateSecuence");
        
        XmlElement element = xmlDocument.CreateElement("Secuence5");
        xmlDocument.DocumentElement.AppendChild(element);

        XmlElement node1 = xmlDocument.CreateElement("Time");
        node1.InnerText = 5.ToString();
        element.AppendChild(node1);

        element.InnerXml = element.InnerXml.Replace(node1.OuterXml,
        "\n    " + node1.OuterXml + " \n    ");

        XmlElement node2 = xmlDocument.CreateElement("Action");
        node2.InnerText = "Button2";
        
        element.AppendChild(node2);

        element.InnerXml = element.InnerXml.Replace(node2.OuterXml, node2.OuterXml + "   \n  ");

        
    }

    public void SaveSong()
    {
        Debug.Log("Save");
        xmlDocument.PreserveWhitespace = true;
        xmlDocument.Save("Assets/Resources/XML/Songs.xml");
    }
}
