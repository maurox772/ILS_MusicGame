﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongManager : MonoBehaviour
{
    private SongXMLManager song;

    private void Start()
    {
        song = new SongXMLManager();
        song.LoadXML();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("ChangeValue");
            song.CreateSecuence();
            song.SaveSong();
        }
    }
}
