﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ILS_Button : MonoBehaviour
{
    private bool CD = true;
    private GameObject child;

    private void Start()
    {
        child = transform.GetChild(0).gameObject;
    }

    public void ActiveAnimation(float lenght)
    {
        LeanTween.scale(child, new Vector3(0.01f, 0.01f, 0.01f), lenght).setOnComplete(EnableButton);
    }

    private void EnableButton()
    {
        CD = true;
        child.transform.localScale = new Vector3(0.02533807f, 0.0253043f, 0.3740521f);
    }

    void OnMouseDown()
    {
        if (!CD)
            return;
        CD = false;
        StartCoroutine(ButtonCD());
        print(name);        
    }

    IEnumerator ButtonCD()
    {
        yield return new WaitForSeconds(0.34f);
    }
}