﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ILS_Disc : MonoBehaviour
{
    public void ActiveRotateAnimation(Vector3 direction,float time,float size)
    {
        LeanTween.rotateAroundLocal(this.gameObject, direction, size  , time).setOnComplete(EnableDisc);
    }
    //private void OnMouseDown()
    //{
    //    ActiveRotateAnimation();
    //    //this.gameObject.fillAmount = (obj.transform.rotation.eulerAngles.z + 180f) / 360f;
    //}
    private  void EnableDisc()
    {
        this.gameObject.transform.rotation = Quaternion.Euler(15, 0, 0);
    }
}