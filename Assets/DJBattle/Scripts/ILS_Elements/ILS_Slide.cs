﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ILS_Slide : MonoBehaviour
{
    public Slider[] slide;
    private GameObject child;

    private void Awake()
    {
        slide = GetComponentsInChildren<Slider>();
        DisableSlides();
    }

    public void DisableSlides()
    {
        for (int i = 0; i <= 3; i++)
        {
            child = transform.GetChild(i).gameObject;
            child.SetActive(false);
        }
    }

    private void Start()
    {
        Invoke("Changed", 1f);
    }

    private void Changed()
    {
        StartCoroutine(ValueToChange());
    }

    IEnumerator ValueToChange()
    {
        int i = Random.Range(0, slide.Length);
        child = transform.GetChild(i).gameObject;
        child.SetActive(true);
        Debug.Log("algo");
        while (slide[i].value < 100)
        {
            slide[i].value += 2f;
            yield return null;
        }
        slide[i].value = 0;
        child.SetActive(false);
        Changed();
    }
}
