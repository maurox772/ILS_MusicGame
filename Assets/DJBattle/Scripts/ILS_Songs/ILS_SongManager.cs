﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ILS_SongManager : MonoBehaviour
{
    public AudioSource song;
    private ILS_ConsoleBehavior console;
    public ILS_Button[] button;
    public ILS_Disc[] disc;

    private void Awake()
    {
        console = GetComponent<ILS_ConsoleBehavior>();
        button = GetComponentsInChildren<ILS_Button>();
        disc = GetComponentsInChildren<ILS_Disc>();
    }

    public void StartSong()
    {
        StartCoroutine(SongTime());
    }

    public void SendAnimation(float lenght1, float lenght2, float lenght3)
    {
            button[0].ActiveAnimation(lenght1);
            button[1].ActiveAnimation(lenght2);
            button[2].ActiveAnimation(lenght3);
    }

    public void SendAnimationDisc()
    {
        for (int i = 0; i < button.Length; i++)
        {
            disc[i].ActiveRotateAnimation(Vector3.up, 1f,-50f);
        }
    }

    IEnumerator SongTime()
    {
        while (Mathf.Round(song.time * 1) / 1 < 505)
        {
            Debug.Log(Mathf.Round(song.time * 1) / 1);
            if ((Mathf.Round(song.time * 1 / 1) == 2))
            {
                SendAnimation(1f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 4))
            {
                SendAnimation(0f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 6))
            {
                SendAnimation(0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 8))
            {
                SendAnimation(0f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 10))
            {
                SendAnimation(1f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 12))
            {
                SendAnimation(0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 14))
            {
                SendAnimation(1f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 16))
            {
                SendAnimation(0f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 18))
            {
                SendAnimation(0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 20))
            {
                SendAnimation(1f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 22))
            {
                SendAnimation(1f, 0.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 24))
            {
                SendAnimation(1f, 0.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 26))
            {
                SendAnimation(0.5f, 0f, 1f);
            }

            yield return new WaitForSeconds(1);
        }

    }
}
