﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ILS_ConsoleBehavior : MonoBehaviour
{
    public ILS_Enable[] consoles;

    void Awake()
    {
        consoles = GetComponentsInChildren<ILS_Enable>();
    }

    public void Initialize()
    {
        for (int i = 0; i < consoles.Length; i++)
        {
            consoles[i].Disable();
        }        
    }

    public void EnableConsole(ConsoleStates State)
    {
        switch (State)
        {
            case ConsoleStates.Red:
                consoles[0].Enable();
                consoles[1].Disable();
                break;
            case ConsoleStates.Blue:
                consoles[1].Enable();
                consoles[0].Disable();
                break;
            case ConsoleStates.Pink:
                consoles[2].Enable();
                consoles[1].Disable();
                break;
            default:
                break;
        }
    }
}
