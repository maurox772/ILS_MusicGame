﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ILS_GameManager : MonoBehaviour
{
    public ILS_ConsoleBehavior console;
    public ILS_SongManager songManager;

    void Start()
    {
        console.Initialize();
        console.EnableConsole(ConsoleStates.Blue);
        Invoke("ActiveAnimation", 2);
    }

    private void ActiveAnimation()
    {
        Debug.Log("Elimboke");
        songManager.StartSong();
        songManager.SendAnimationDisc();
    }
    
}
