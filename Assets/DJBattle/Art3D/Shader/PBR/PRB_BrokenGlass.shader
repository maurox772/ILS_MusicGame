// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:32719,y:32712,varname:node_2865,prsc:2|diff-6343-OUT,spec-358-OUT,gloss-1813-OUT,normal-5964-RGB,emission-8343-OUT,alpha-1067-OUT;n:type:ShaderForge.SFN_Multiply,id:6343,x:32275,y:32045,varname:node_6343,prsc:2|A-7736-RGB,B-6665-RGB;n:type:ShaderForge.SFN_Color,id:6665,x:32082,y:32138,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7736,x:32082,y:31953,varname:_MainTex,prsc:2,tex:bebd13a6f784b8b4eaa8a4d85853ab78,ntxv:0,isnm:False|TEX-9660-TEX;n:type:ShaderForge.SFN_Tex2d,id:5964,x:32309,y:32545,ptovrint:True,ptlb:Normal Map,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:30b396f87eb062c489cfe0fcc57e72f3,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Slider,id:358,x:32152,y:32347,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:_Metallic,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:1813,x:32152,y:32449,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Gloss,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_VertexColor,id:8775,x:32664,y:33540,varname:node_8775,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1067,x:33123,y:33627,varname:node_1067,prsc:2|A-8775-A,B-1350-OUT;n:type:ShaderForge.SFN_Tex2d,id:4056,x:31239,y:32355,varname:_Glass1Texture,prsc:2,tex:bebd13a6f784b8b4eaa8a4d85853ab78,ntxv:0,isnm:False|TEX-9660-TEX;n:type:ShaderForge.SFN_Slider,id:8000,x:31082,y:32558,ptovrint:False,ptlb:Glass 1 Intenisty,ptin:_Glass1Intenisty,varname:_Glass1Intenisty,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:5;n:type:ShaderForge.SFN_Multiply,id:2887,x:31535,y:32441,varname:node_2887,prsc:2|A-4056-RGB,B-8000-OUT,C-6985-OUT;n:type:ShaderForge.SFN_Tex2d,id:3003,x:31239,y:32743,varname:_Glass2,prsc:2,tex:bebd13a6f784b8b4eaa8a4d85853ab78,ntxv:0,isnm:False|UVIN-1251-OUT,TEX-9660-TEX;n:type:ShaderForge.SFN_Slider,id:5262,x:30870,y:33079,ptovrint:False,ptlb:Glass 2 Intensity,ptin:_Glass2Intensity,varname:_Glass2Intensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:5;n:type:ShaderForge.SFN_Multiply,id:6737,x:31541,y:32919,varname:node_6737,prsc:2|A-3003-RGB,B-5262-OUT,C-40-OUT;n:type:ShaderForge.SFN_Rotator,id:8499,x:30792,y:32737,varname:node_8499,prsc:2|UVIN-4628-UVOUT,ANG-6555-OUT;n:type:ShaderForge.SFN_TexCoord,id:4628,x:30536,y:32635,varname:node_4628,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Panner,id:6183,x:30792,y:32875,varname:node_6183,prsc:2,spu:1,spv:1|UVIN-4628-UVOUT,DIST-5720-OUT;n:type:ShaderForge.SFN_Multiply,id:1251,x:31026,y:32825,varname:node_1251,prsc:2|A-8499-UVOUT,B-6183-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:5720,x:30536,y:32981,ptovrint:False,ptlb:Glass 2 UV Offset,ptin:_Glass2UVOffset,varname:_Glass2UVOffset,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_ToggleProperty,id:40,x:31027,y:33249,ptovrint:False,ptlb:Glass 2 Toggle,ptin:_Glass2Toggle,varname:_Glass2Toggle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_Tex2d,id:5568,x:31237,y:33399,varname:_Glass3,prsc:2,tex:bebd13a6f784b8b4eaa8a4d85853ab78,ntxv:0,isnm:False|UVIN-4868-OUT,TEX-9449-TEX;n:type:ShaderForge.SFN_Slider,id:7501,x:30866,y:33772,ptovrint:False,ptlb:Glass 3 Intensity,ptin:_Glass3Intensity,varname:_Glass3Intensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:5;n:type:ShaderForge.SFN_Multiply,id:5258,x:31537,y:33612,varname:node_5258,prsc:2|A-5568-RGB,B-7501-OUT,C-2978-OUT;n:type:ShaderForge.SFN_Rotator,id:4698,x:30788,y:33430,varname:node_4698,prsc:2|UVIN-735-UVOUT,ANG-6695-OUT;n:type:ShaderForge.SFN_TexCoord,id:735,x:30531,y:33328,varname:node_735,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Panner,id:1867,x:30788,y:33569,varname:node_1867,prsc:2,spu:1,spv:1|UVIN-735-UVOUT,DIST-2208-OUT;n:type:ShaderForge.SFN_Multiply,id:4868,x:31022,y:33518,varname:node_4868,prsc:2|A-4698-UVOUT,B-1867-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:2208,x:30531,y:33674,ptovrint:False,ptlb:Glass 3 UV Offset,ptin:_Glass3UVOffset,varname:_Glass3UVOffset,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.6;n:type:ShaderForge.SFN_ToggleProperty,id:2978,x:31023,y:33897,ptovrint:False,ptlb:Glass 3 Toggle,ptin:_Glass3Toggle,varname:_Glass3Toggle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_Add,id:8343,x:31821,y:33044,varname:node_8343,prsc:2|A-2887-OUT,B-6737-OUT,C-5258-OUT,D-8080-OUT,E-9691-OUT;n:type:ShaderForge.SFN_Slider,id:6695,x:30431,y:33561,ptovrint:False,ptlb:Glass 3 Rot Angle,ptin:_Glass3RotAngle,varname:_Glass3RotAngle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:58.4,max:180;n:type:ShaderForge.SFN_Slider,id:6555,x:30408,y:32882,ptovrint:False,ptlb:Glass 2 Rot Angle,ptin:_Glass2RotAngle,varname:_Glass2RotAngle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:90,max:180;n:type:ShaderForge.SFN_Tex2d,id:5952,x:31240,y:34045,varname:_Glass3Texture_copy,prsc:2,tex:bebd13a6f784b8b4eaa8a4d85853ab78,ntxv:0,isnm:False|UVIN-1108-OUT,TEX-9449-TEX;n:type:ShaderForge.SFN_Slider,id:4060,x:30869,y:34418,ptovrint:False,ptlb:Glass 4 Intensity,ptin:_Glass4Intensity,varname:_Glass4Intensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:5;n:type:ShaderForge.SFN_Multiply,id:8080,x:31540,y:34258,varname:node_8080,prsc:2|A-5952-RGB,B-4060-OUT,C-6942-OUT;n:type:ShaderForge.SFN_Rotator,id:9523,x:30791,y:34076,varname:node_9523,prsc:2|UVIN-4321-UVOUT,ANG-4384-OUT;n:type:ShaderForge.SFN_TexCoord,id:4321,x:30534,y:33974,varname:node_4321,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Panner,id:6103,x:30791,y:34215,varname:node_6103,prsc:2,spu:1,spv:1|UVIN-4321-UVOUT,DIST-7709-OUT;n:type:ShaderForge.SFN_Multiply,id:1108,x:31025,y:34164,varname:node_1108,prsc:2|A-9523-UVOUT,B-6103-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:7709,x:30534,y:34320,ptovrint:False,ptlb:Glass 4 UV Offset,ptin:_Glass4UVOffset,varname:_Glass4UVOffset,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.2;n:type:ShaderForge.SFN_ToggleProperty,id:6942,x:31026,y:34543,ptovrint:False,ptlb:Glass 4 Toggle,ptin:_Glass4Toggle,varname:_Glass4Toggle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_Slider,id:4384,x:30434,y:34207,ptovrint:False,ptlb:Glass 4 Rot Angle,ptin:_Glass4RotAngle,varname:_Glass4RotAngle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:85.33538,max:180;n:type:ShaderForge.SFN_Tex2d,id:2641,x:31241,y:34736,varname:_Glass3Texture_copy_copy,prsc:2,tex:bebd13a6f784b8b4eaa8a4d85853ab78,ntxv:0,isnm:False|UVIN-80-OUT,TEX-9449-TEX;n:type:ShaderForge.SFN_Slider,id:8013,x:30870,y:35109,ptovrint:False,ptlb:Glass 5 Intensity,ptin:_Glass5Intensity,varname:_Glass5Intensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:5;n:type:ShaderForge.SFN_Multiply,id:9691,x:31541,y:34949,varname:node_9691,prsc:2|A-2641-RGB,B-8013-OUT,C-4531-OUT;n:type:ShaderForge.SFN_Rotator,id:3944,x:30792,y:34767,varname:node_3944,prsc:2|UVIN-7230-UVOUT,ANG-8625-OUT;n:type:ShaderForge.SFN_TexCoord,id:7230,x:30535,y:34665,varname:node_7230,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Panner,id:495,x:30792,y:34906,varname:node_495,prsc:2,spu:1,spv:1|UVIN-7230-UVOUT,DIST-6245-OUT;n:type:ShaderForge.SFN_Multiply,id:80,x:31026,y:34855,varname:node_80,prsc:2|A-3944-UVOUT,B-495-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:6245,x:30535,y:35011,ptovrint:False,ptlb:Glass 5 UV Offset,ptin:_Glass5UVOffset,varname:_Glass5UVOffset,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_ToggleProperty,id:4531,x:31027,y:35234,ptovrint:False,ptlb:Glass 5 Toggle,ptin:_Glass5Toggle,varname:_Glass5Toggle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_Slider,id:8625,x:30435,y:34898,ptovrint:False,ptlb:Glass 5 Rot Angle,ptin:_Glass5RotAngle,varname:_Glass5RotAngle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:103.6469,max:180;n:type:ShaderForge.SFN_Tex2dAsset,id:9660,x:29885,y:33658,ptovrint:False,ptlb:Base_Texture,ptin:_Base_Texture,varname:_Base_Texture,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bebd13a6f784b8b4eaa8a4d85853ab78,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:3011,x:32390,y:33713,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:_Opacity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.0375;n:type:ShaderForge.SFN_Tex2dAsset,id:9449,x:29923,y:34363,ptovrint:False,ptlb:Base_Texture2,ptin:_Base_Texture2,varname:_Base_Texture2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bebd13a6f784b8b4eaa8a4d85853ab78,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ToggleProperty,id:6985,x:31227,y:32639,ptovrint:False,ptlb:Glass 1 Toggle,ptin:_Glass1Toggle,varname:_Glass1Toggle,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_Add,id:9126,x:32390,y:33771,varname:node_9126,prsc:2|A-6985-OUT,B-40-OUT,C-2978-OUT,D-6942-OUT;n:type:ShaderForge.SFN_Multiply,id:1350,x:32664,y:33753,varname:node_1350,prsc:2|A-3011-OUT,B-9126-OUT,C-846-OUT;n:type:ShaderForge.SFN_Vector1,id:9998,x:32105,y:34825,varname:node_9998,prsc:2,v1:2.333333;n:type:ShaderForge.SFN_Add,id:846,x:32328,y:34643,varname:node_846,prsc:2|A-4531-OUT,B-9998-OUT;proporder:6985-9660-6665-5964-358-1813-3011-8000-40-5262-6555-5720-2978-7501-6695-2208-6942-4060-4384-7709-4531-8013-8625-6245-9449;pass:END;sub:END;*/

Shader "Shader Forge/PRB_BrokenGlass" {
    Properties {
        [MaterialToggle] _Glass1Toggle ("Glass 1 Toggle", Float ) = 1
        _Base_Texture ("Base_Texture", 2D) = "white" {}
        _Color ("Color", Color) = (0,0,0,1)
        _BumpMap ("Normal Map", 2D) = "bump" {}
        _Metallic ("Metallic", Range(0, 1)) = 0
        _Gloss ("Gloss", Range(0, 1)) = 0
        _Opacity ("Opacity", Float ) = 0.0375
        _Glass1Intenisty ("Glass 1 Intenisty", Range(0, 5)) = 1
        [MaterialToggle] _Glass2Toggle ("Glass 2 Toggle", Float ) = 1
        _Glass2Intensity ("Glass 2 Intensity", Range(0, 5)) = 1
        _Glass2RotAngle ("Glass 2 Rot Angle", Range(0, 180)) = 90
        _Glass2UVOffset ("Glass 2 UV Offset", Float ) = 0.5
        [MaterialToggle] _Glass3Toggle ("Glass 3 Toggle", Float ) = 1
        _Glass3Intensity ("Glass 3 Intensity", Range(0, 5)) = 1
        _Glass3RotAngle ("Glass 3 Rot Angle", Range(0, 180)) = 58.4
        _Glass3UVOffset ("Glass 3 UV Offset", Float ) = 0.6
        [MaterialToggle] _Glass4Toggle ("Glass 4 Toggle", Float ) = 1
        _Glass4Intensity ("Glass 4 Intensity", Range(0, 5)) = 1
        _Glass4RotAngle ("Glass 4 Rot Angle", Range(0, 180)) = 85.33538
        _Glass4UVOffset ("Glass 4 UV Offset", Float ) = 1.2
        [MaterialToggle] _Glass5Toggle ("Glass 5 Toggle", Float ) = 1
        _Glass5Intensity ("Glass 5 Intensity", Range(0, 5)) = 1
        _Glass5RotAngle ("Glass 5 Rot Angle", Range(0, 180)) = 103.6469
        _Glass5UVOffset ("Glass 5 UV Offset", Float ) = 1.5
        _Base_Texture2 ("Base_Texture2", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform float _Glass1Intenisty;
            uniform float _Glass2Intensity;
            uniform float _Glass2UVOffset;
            uniform fixed _Glass2Toggle;
            uniform float _Glass3Intensity;
            uniform float _Glass3UVOffset;
            uniform fixed _Glass3Toggle;
            uniform float _Glass3RotAngle;
            uniform float _Glass2RotAngle;
            uniform float _Glass4Intensity;
            uniform float _Glass4UVOffset;
            uniform fixed _Glass4Toggle;
            uniform float _Glass4RotAngle;
            uniform float _Glass5Intensity;
            uniform float _Glass5UVOffset;
            uniform fixed _Glass5Toggle;
            uniform float _Glass5RotAngle;
            uniform sampler2D _Base_Texture; uniform float4 _Base_Texture_ST;
            uniform float _Opacity;
            uniform sampler2D _Base_Texture2; uniform float4 _Base_Texture2_ST;
            uniform fixed _Glass1Toggle;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(7)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD8;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.vertexColor = v.vertexColor;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = _BumpMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float perceptualRoughness = 1.0 - _Gloss;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float4 _MainTex = tex2D(_Base_Texture,TRANSFORM_TEX(i.uv0, _Base_Texture));
                float3 diffuseColor = (_MainTex.rgb*_Color.rgb); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                half surfaceReduction;
                #ifdef UNITY_COLORSPACE_GAMMA
                    surfaceReduction = 1.0-0.28*roughness*perceptualRoughness;
                #else
                    surfaceReduction = 1.0/(roughness*roughness + 1.0);
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                indirectSpecular *= surfaceReduction;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float4 _Glass1Texture = tex2D(_Base_Texture,TRANSFORM_TEX(i.uv0, _Base_Texture));
                float node_8499_ang = _Glass2RotAngle;
                float node_8499_spd = 1.0;
                float node_8499_cos = cos(node_8499_spd*node_8499_ang);
                float node_8499_sin = sin(node_8499_spd*node_8499_ang);
                float2 node_8499_piv = float2(0.5,0.5);
                float2 node_8499 = (mul(i.uv0-node_8499_piv,float2x2( node_8499_cos, -node_8499_sin, node_8499_sin, node_8499_cos))+node_8499_piv);
                float2 node_1251 = (node_8499*(i.uv0+_Glass2UVOffset*float2(1,1)));
                float4 _Glass2 = tex2D(_Base_Texture,TRANSFORM_TEX(node_1251, _Base_Texture));
                float node_4698_ang = _Glass3RotAngle;
                float node_4698_spd = 1.0;
                float node_4698_cos = cos(node_4698_spd*node_4698_ang);
                float node_4698_sin = sin(node_4698_spd*node_4698_ang);
                float2 node_4698_piv = float2(0.5,0.5);
                float2 node_4698 = (mul(i.uv0-node_4698_piv,float2x2( node_4698_cos, -node_4698_sin, node_4698_sin, node_4698_cos))+node_4698_piv);
                float2 node_4868 = (node_4698*(i.uv0+_Glass3UVOffset*float2(1,1)));
                float4 _Glass3 = tex2D(_Base_Texture2,TRANSFORM_TEX(node_4868, _Base_Texture2));
                float node_9523_ang = _Glass4RotAngle;
                float node_9523_spd = 1.0;
                float node_9523_cos = cos(node_9523_spd*node_9523_ang);
                float node_9523_sin = sin(node_9523_spd*node_9523_ang);
                float2 node_9523_piv = float2(0.5,0.5);
                float2 node_9523 = (mul(i.uv0-node_9523_piv,float2x2( node_9523_cos, -node_9523_sin, node_9523_sin, node_9523_cos))+node_9523_piv);
                float2 node_1108 = (node_9523*(i.uv0+_Glass4UVOffset*float2(1,1)));
                float4 _Glass3Texture_copy = tex2D(_Base_Texture2,TRANSFORM_TEX(node_1108, _Base_Texture2));
                float node_3944_ang = _Glass5RotAngle;
                float node_3944_spd = 1.0;
                float node_3944_cos = cos(node_3944_spd*node_3944_ang);
                float node_3944_sin = sin(node_3944_spd*node_3944_ang);
                float2 node_3944_piv = float2(0.5,0.5);
                float2 node_3944 = (mul(i.uv0-node_3944_piv,float2x2( node_3944_cos, -node_3944_sin, node_3944_sin, node_3944_cos))+node_3944_piv);
                float2 node_80 = (node_3944*(i.uv0+_Glass5UVOffset*float2(1,1)));
                float4 _Glass3Texture_copy_copy = tex2D(_Base_Texture2,TRANSFORM_TEX(node_80, _Base_Texture2));
                float3 emissive = ((_Glass1Texture.rgb*_Glass1Intenisty*_Glass1Toggle)+(_Glass2.rgb*_Glass2Intensity*_Glass2Toggle)+(_Glass3.rgb*_Glass3Intensity*_Glass3Toggle)+(_Glass3Texture_copy.rgb*_Glass4Intensity*_Glass4Toggle)+(_Glass3Texture_copy_copy.rgb*_Glass5Intensity*_Glass5Toggle));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,(i.vertexColor.a*(_Opacity*(_Glass1Toggle+_Glass2Toggle+_Glass3Toggle+_Glass4Toggle)*(_Glass5Toggle+2.333333))));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform float _Glass1Intenisty;
            uniform float _Glass2Intensity;
            uniform float _Glass2UVOffset;
            uniform fixed _Glass2Toggle;
            uniform float _Glass3Intensity;
            uniform float _Glass3UVOffset;
            uniform fixed _Glass3Toggle;
            uniform float _Glass3RotAngle;
            uniform float _Glass2RotAngle;
            uniform float _Glass4Intensity;
            uniform float _Glass4UVOffset;
            uniform fixed _Glass4Toggle;
            uniform float _Glass4RotAngle;
            uniform float _Glass5Intensity;
            uniform float _Glass5UVOffset;
            uniform fixed _Glass5Toggle;
            uniform float _Glass5RotAngle;
            uniform sampler2D _Base_Texture; uniform float4 _Base_Texture_ST;
            uniform float _Opacity;
            uniform sampler2D _Base_Texture2; uniform float4 _Base_Texture2_ST;
            uniform fixed _Glass1Toggle;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = _BumpMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float perceptualRoughness = 1.0 - _Gloss;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float4 _MainTex = tex2D(_Base_Texture,TRANSFORM_TEX(i.uv0, _Base_Texture));
                float3 diffuseColor = (_MainTex.rgb*_Color.rgb); // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * (i.vertexColor.a*(_Opacity*(_Glass1Toggle+_Glass2Toggle+_Glass3Toggle+_Glass4Toggle)*(_Glass5Toggle+2.333333))),0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform float _Glass1Intenisty;
            uniform float _Glass2Intensity;
            uniform float _Glass2UVOffset;
            uniform fixed _Glass2Toggle;
            uniform float _Glass3Intensity;
            uniform float _Glass3UVOffset;
            uniform fixed _Glass3Toggle;
            uniform float _Glass3RotAngle;
            uniform float _Glass2RotAngle;
            uniform float _Glass4Intensity;
            uniform float _Glass4UVOffset;
            uniform fixed _Glass4Toggle;
            uniform float _Glass4RotAngle;
            uniform float _Glass5Intensity;
            uniform float _Glass5UVOffset;
            uniform fixed _Glass5Toggle;
            uniform float _Glass5RotAngle;
            uniform sampler2D _Base_Texture; uniform float4 _Base_Texture_ST;
            uniform sampler2D _Base_Texture2; uniform float4 _Base_Texture2_ST;
            uniform fixed _Glass1Toggle;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float4 _Glass1Texture = tex2D(_Base_Texture,TRANSFORM_TEX(i.uv0, _Base_Texture));
                float node_8499_ang = _Glass2RotAngle;
                float node_8499_spd = 1.0;
                float node_8499_cos = cos(node_8499_spd*node_8499_ang);
                float node_8499_sin = sin(node_8499_spd*node_8499_ang);
                float2 node_8499_piv = float2(0.5,0.5);
                float2 node_8499 = (mul(i.uv0-node_8499_piv,float2x2( node_8499_cos, -node_8499_sin, node_8499_sin, node_8499_cos))+node_8499_piv);
                float2 node_1251 = (node_8499*(i.uv0+_Glass2UVOffset*float2(1,1)));
                float4 _Glass2 = tex2D(_Base_Texture,TRANSFORM_TEX(node_1251, _Base_Texture));
                float node_4698_ang = _Glass3RotAngle;
                float node_4698_spd = 1.0;
                float node_4698_cos = cos(node_4698_spd*node_4698_ang);
                float node_4698_sin = sin(node_4698_spd*node_4698_ang);
                float2 node_4698_piv = float2(0.5,0.5);
                float2 node_4698 = (mul(i.uv0-node_4698_piv,float2x2( node_4698_cos, -node_4698_sin, node_4698_sin, node_4698_cos))+node_4698_piv);
                float2 node_4868 = (node_4698*(i.uv0+_Glass3UVOffset*float2(1,1)));
                float4 _Glass3 = tex2D(_Base_Texture2,TRANSFORM_TEX(node_4868, _Base_Texture2));
                float node_9523_ang = _Glass4RotAngle;
                float node_9523_spd = 1.0;
                float node_9523_cos = cos(node_9523_spd*node_9523_ang);
                float node_9523_sin = sin(node_9523_spd*node_9523_ang);
                float2 node_9523_piv = float2(0.5,0.5);
                float2 node_9523 = (mul(i.uv0-node_9523_piv,float2x2( node_9523_cos, -node_9523_sin, node_9523_sin, node_9523_cos))+node_9523_piv);
                float2 node_1108 = (node_9523*(i.uv0+_Glass4UVOffset*float2(1,1)));
                float4 _Glass3Texture_copy = tex2D(_Base_Texture2,TRANSFORM_TEX(node_1108, _Base_Texture2));
                float node_3944_ang = _Glass5RotAngle;
                float node_3944_spd = 1.0;
                float node_3944_cos = cos(node_3944_spd*node_3944_ang);
                float node_3944_sin = sin(node_3944_spd*node_3944_ang);
                float2 node_3944_piv = float2(0.5,0.5);
                float2 node_3944 = (mul(i.uv0-node_3944_piv,float2x2( node_3944_cos, -node_3944_sin, node_3944_sin, node_3944_cos))+node_3944_piv);
                float2 node_80 = (node_3944*(i.uv0+_Glass5UVOffset*float2(1,1)));
                float4 _Glass3Texture_copy_copy = tex2D(_Base_Texture2,TRANSFORM_TEX(node_80, _Base_Texture2));
                o.Emission = ((_Glass1Texture.rgb*_Glass1Intenisty*_Glass1Toggle)+(_Glass2.rgb*_Glass2Intensity*_Glass2Toggle)+(_Glass3.rgb*_Glass3Intensity*_Glass3Toggle)+(_Glass3Texture_copy.rgb*_Glass4Intensity*_Glass4Toggle)+(_Glass3Texture_copy_copy.rgb*_Glass5Intensity*_Glass5Toggle));
                
                float4 _MainTex = tex2D(_Base_Texture,TRANSFORM_TEX(i.uv0, _Base_Texture));
                float3 diffColor = (_MainTex.rgb*_Color.rgb);
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, _Metallic, specColor, specularMonochrome );
                float roughness = 1.0 - _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
