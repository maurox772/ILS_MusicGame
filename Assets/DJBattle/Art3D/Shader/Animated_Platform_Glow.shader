// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.07671929,fgcg:0.3161765,fgcb:0.1279136,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:7980,x:33506,y:32608,varname:node_7980,prsc:2|emission-5021-OUT,alpha-1684-OUT,voffset-3014-OUT,tess-5854-OUT;n:type:ShaderForge.SFN_TexCoord,id:8960,x:30838,y:33094,varname:node_8960,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Color,id:300,x:32883,y:32396,ptovrint:False,ptlb:Glow_Color,ptin:_Glow_Color,varname:node_300,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:5021,x:33182,y:32396,varname:node_5021,prsc:2|A-300-RGB,B-1717-OUT;n:type:ShaderForge.SFN_Length,id:9751,x:31161,y:32915,varname:node_9751,prsc:2|IN-8960-V;n:type:ShaderForge.SFN_ComponentMask,id:3175,x:31416,y:32915,varname:node_3175,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-9751-OUT;n:type:ShaderForge.SFN_Multiply,id:7250,x:32120,y:32894,varname:node_7250,prsc:2|A-3777-OUT,B-514-OUT;n:type:ShaderForge.SFN_Slider,id:4295,x:32120,y:32766,ptovrint:False,ptlb:Frecuency_01,ptin:_Frecuency_01,varname:_Frecuency_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:5;n:type:ShaderForge.SFN_Add,id:3777,x:31879,y:32913,varname:node_3777,prsc:2|A-4402-OUT,B-5591-OUT;n:type:ShaderForge.SFN_Time,id:4236,x:31164,y:33346,varname:node_4236,prsc:2;n:type:ShaderForge.SFN_Sin,id:2613,x:32290,y:32894,varname:node_2613,prsc:2|IN-7250-OUT;n:type:ShaderForge.SFN_Tau,id:514,x:31765,y:33120,varname:node_514,prsc:2;n:type:ShaderForge.SFN_Clamp01,id:4402,x:31589,y:32915,varname:node_4402,prsc:2|IN-3175-OUT;n:type:ShaderForge.SFN_Multiply,id:5591,x:31402,y:33098,varname:node_5591,prsc:2|A-4236-T,B-1261-OUT;n:type:ShaderForge.SFN_Slider,id:4737,x:30473,y:33379,ptovrint:False,ptlb:Wave_Rate,ptin:_Wave_Rate,varname:node_4737,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8,max:1;n:type:ShaderForge.SFN_RemapRange,id:1261,x:30849,y:33382,varname:node_1261,prsc:2,frmn:0,frmx:1,tomn:1,tomx:-1|IN-4737-OUT;n:type:ShaderForge.SFN_NormalVector,id:2778,x:33056,y:33470,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:3014,x:33279,y:33451,varname:node_3014,prsc:2|A-1717-OUT,B-2778-OUT,C-287-OUT;n:type:ShaderForge.SFN_Slider,id:287,x:32899,y:33677,ptovrint:False,ptlb:Offset_Intensity,ptin:_Offset_Intensity,varname:node_287,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.025,max:1;n:type:ShaderForge.SFN_Vector1,id:5854,x:33279,y:33596,varname:node_5854,prsc:2,v1:3.5;n:type:ShaderForge.SFN_Slider,id:3527,x:32149,y:33456,ptovrint:False,ptlb:Distortion,ptin:_Distortion,varname:node_3527,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4882385,max:1;n:type:ShaderForge.SFN_Tex2d,id:9426,x:32743,y:33310,ptovrint:False,ptlb:Opacity_Clip,ptin:_Opacity_Clip,varname:node_9426,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b36d09538fc343446bf6ca1f6c3b908b,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1684,x:33234,y:32881,varname:node_1684,prsc:2|A-1717-OUT,B-5169-OUT;n:type:ShaderForge.SFN_Slider,id:9984,x:32679,y:33529,ptovrint:False,ptlb:Opacity_Streght,ptin:_Opacity_Streght,varname:node_9984,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Lerp,id:5169,x:32942,y:33258,varname:node_5169,prsc:2|A-1556-OUT,B-9426-R,T-9984-OUT;n:type:ShaderForge.SFN_Vector1,id:1556,x:32754,y:33234,varname:node_1556,prsc:2,v1:1;n:type:ShaderForge.SFN_Length,id:5425,x:31150,y:33505,varname:node_5425,prsc:2|IN-8960-V;n:type:ShaderForge.SFN_ComponentMask,id:1445,x:31405,y:33505,varname:node_1445,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-5425-OUT;n:type:ShaderForge.SFN_Multiply,id:393,x:32118,y:33062,varname:node_393,prsc:2|A-796-OUT,B-514-OUT;n:type:ShaderForge.SFN_Add,id:796,x:31868,y:33503,varname:node_796,prsc:2|A-4576-OUT,B-7490-OUT;n:type:ShaderForge.SFN_Clamp01,id:4576,x:31642,y:33515,varname:node_4576,prsc:2|IN-1445-OUT;n:type:ShaderForge.SFN_Multiply,id:7490,x:31391,y:33698,varname:node_7490,prsc:2|A-4236-T,B-1261-OUT;n:type:ShaderForge.SFN_Cos,id:3042,x:32290,y:33062,varname:node_3042,prsc:2|IN-393-OUT;n:type:ShaderForge.SFN_Slider,id:1659,x:32133,y:33268,ptovrint:False,ptlb:Frecuency_02,ptin:_Frecuency_02,varname:node_1659,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:5;n:type:ShaderForge.SFN_Clamp01,id:1717,x:32982,y:32893,varname:node_1717,prsc:2|IN-5243-OUT;n:type:ShaderForge.SFN_Multiply,id:3876,x:32492,y:32862,varname:node_3876,prsc:2|A-4295-OUT,B-2613-OUT;n:type:ShaderForge.SFN_Multiply,id:5612,x:32492,y:33013,varname:node_5612,prsc:2|A-3042-OUT,B-1659-OUT;n:type:ShaderForge.SFN_Add,id:5243,x:32731,y:32891,varname:node_5243,prsc:2|A-3876-OUT,B-5612-OUT;proporder:300-4295-1659-4737-3527-9984-9426-287;pass:END;sub:END;*/

Shader "Custom/Animated_Platform_Glow" {
    Properties {
        _Glow_Color ("Glow_Color", Color) = (0,1,1,1)
        _Frecuency_01 ("Frecuency_01", Range(0, 5)) = 0
        _Frecuency_02 ("Frecuency_02", Range(0, 5)) = 0
        _Wave_Rate ("Wave_Rate", Range(0, 1)) = 0.8
        _Distortion ("Distortion", Range(0, 1)) = 0.4882385
        _Opacity_Streght ("Opacity_Streght", Range(0, 1)) = 1
        _Opacity_Clip ("Opacity_Clip", 2D) = "white" {}
        _Offset_Intensity ("Offset_Intensity", Range(0, 1)) = 0.025
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "Tessellation.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x xboxone ps4 psp2 n3ds wiiu 
            #pragma target 5.0
            uniform float4 _Glow_Color;
            uniform float _Frecuency_01;
            uniform float _Wave_Rate;
            uniform float _Offset_Intensity;
            uniform sampler2D _Opacity_Clip; uniform float4 _Opacity_Clip_ST;
            uniform float _Opacity_Streght;
            uniform float _Frecuency_02;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_4236 = _Time;
                float node_1261 = (_Wave_Rate*-2.0+1.0);
                float node_514 = 6.28318530718;
                float node_3876 = (_Frecuency_01*sin(((saturate(length(o.uv0.g).r)+(node_4236.g*node_1261))*node_514)));
                float node_5612 = (cos(((saturate(length(o.uv0.g).r)+(node_4236.g*node_1261))*node_514))*_Frecuency_02);
                float node_1717 = saturate((node_3876+node_5612));
                v.vertex.xyz += (node_1717*v.normal*_Offset_Intensity);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    return o;
                }
                float Tessellation(TessVertex v){
                    return 3.5;
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_4236 = _Time;
                float node_1261 = (_Wave_Rate*-2.0+1.0);
                float node_514 = 6.28318530718;
                float node_3876 = (_Frecuency_01*sin(((saturate(length(i.uv0.g).r)+(node_4236.g*node_1261))*node_514)));
                float node_5612 = (cos(((saturate(length(i.uv0.g).r)+(node_4236.g*node_1261))*node_514))*_Frecuency_02);
                float node_1717 = saturate((node_3876+node_5612));
                float3 emissive = (_Glow_Color.rgb*node_1717);
                float3 finalColor = emissive;
                float4 _Opacity_Clip_var = tex2D(_Opacity_Clip,TRANSFORM_TEX(i.uv0, _Opacity_Clip));
                fixed4 finalRGBA = fixed4(finalColor,(node_1717*lerp(1.0,_Opacity_Clip_var.r,_Opacity_Streght)));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "Tessellation.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x xboxone ps4 psp2 n3ds wiiu 
            #pragma target 5.0
            uniform float _Frecuency_01;
            uniform float _Wave_Rate;
            uniform float _Offset_Intensity;
            uniform float _Frecuency_02;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_4236 = _Time;
                float node_1261 = (_Wave_Rate*-2.0+1.0);
                float node_514 = 6.28318530718;
                float node_3876 = (_Frecuency_01*sin(((saturate(length(o.uv0.g).r)+(node_4236.g*node_1261))*node_514)));
                float node_5612 = (cos(((saturate(length(o.uv0.g).r)+(node_4236.g*node_1261))*node_514))*_Frecuency_02);
                float node_1717 = saturate((node_3876+node_5612));
                v.vertex.xyz += (node_1717*v.normal*_Offset_Intensity);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex {
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord0 : TEXCOORD0;
                };
                struct OutputPatchConstant {
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };
                TessVertex tessvert (VertexInput v) {
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord0 = v.texcoord0;
                    return o;
                }
                float Tessellation(TessVertex v){
                    return 3.5;
                }
                float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2){
                    float tv = Tessellation(v);
                    float tv1 = Tessellation(v1);
                    float tv2 = Tessellation(v2);
                    return float4( tv1+tv2, tv2+tv, tv+tv1, tv+tv1+tv2 ) / float4(2,2,2,3);
                }
                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v) {
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }
                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) {
                    return v[id];
                }
                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) {
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex*bary.x + vi[1].vertex*bary.y + vi[2].vertex*bary.z;
                    v.normal = vi[0].normal*bary.x + vi[1].normal*bary.y + vi[2].normal*bary.z;
                    v.tangent = vi[0].tangent*bary.x + vi[1].tangent*bary.y + vi[2].tangent*bary.z;
                    v.texcoord0 = vi[0].texcoord0*bary.x + vi[1].texcoord0*bary.y + vi[2].texcoord0*bary.z;
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
