// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:32719,y:32712,varname:node_4013,prsc:2|diff-5072-OUT,normal-6468-OUT;n:type:ShaderForge.SFN_Tex2d,id:614,x:32065,y:32266,ptovrint:False,ptlb:Texture Color 2,ptin:_TextureColor2,varname:node_614,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bad833ad20e8e124bb248580c511bdfd,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:5072,x:32312,y:32613,varname:node_5072,prsc:2|A-614-RGB,B-8266-RGB,T-6658-OUT;n:type:ShaderForge.SFN_Tex2d,id:8266,x:32065,y:32454,ptovrint:False,ptlb:Texture Color 1,ptin:_TextureColor1,varname:_Diffuse_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:5ea3c46d0515c454cac099a24ff9193f,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Clamp01,id:6658,x:31996,y:32721,varname:node_6658,prsc:2|IN-8742-OUT;n:type:ShaderForge.SFN_Divide,id:8742,x:31765,y:32721,varname:node_8742,prsc:2|A-352-OUT,B-6035-OUT;n:type:ShaderForge.SFN_Distance,id:352,x:31469,y:32620,varname:node_352,prsc:2|A-419-XYZ,B-4810-XYZ;n:type:ShaderForge.SFN_ViewPosition,id:4810,x:31228,y:32730,varname:node_4810,prsc:2;n:type:ShaderForge.SFN_FragmentPosition,id:419,x:31221,y:32586,varname:node_419,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:6035,x:31413,y:32831,ptovrint:False,ptlb:Transition Distance,ptin:_TransitionDistance,varname:node_6035,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Tex2d,id:5188,x:31376,y:32980,varname:node_5188,prsc:2,tex:a4272ae14cc4e434384bcbfc3484f542,ntxv:3,isnm:True|UVIN-9839-UVOUT,TEX-7157-TEX;n:type:ShaderForge.SFN_Tex2d,id:7095,x:31191,y:33434,varname:_Normal_copy,prsc:2,tex:a4272ae14cc4e434384bcbfc3484f542,ntxv:3,isnm:True|UVIN-9839-UVOUT,TEX-2496-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:7157,x:31141,y:33110,ptovrint:False,ptlb:Normal2,ptin:_Normal2,varname:node_7157,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a4272ae14cc4e434384bcbfc3484f542,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Lerp,id:6468,x:32162,y:32933,varname:node_6468,prsc:2|A-3916-OUT,B-5675-OUT,T-6658-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:2496,x:30913,y:33537,ptovrint:False,ptlb:Normal1,ptin:_Normal1,varname:_Normal2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a4272ae14cc4e434384bcbfc3484f542,ntxv:3,isnm:True;n:type:ShaderForge.SFN_TexCoord,id:9839,x:30688,y:33176,varname:node_9839,prsc:2,uv:2,uaff:False;n:type:ShaderForge.SFN_Lerp,id:3916,x:31688,y:33074,varname:node_3916,prsc:2|A-5188-RGB,B-1655-OUT,T-1863-OUT;n:type:ShaderForge.SFN_Vector3,id:1655,x:31420,y:33133,varname:node_1655,prsc:2,v1:0,v2:0,v3:-1;n:type:ShaderForge.SFN_Slider,id:1863,x:31342,y:33263,ptovrint:False,ptlb:Normal Intensity2,ptin:_NormalIntensity2,varname:_NormalIntensity_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:5675,x:31701,y:33363,varname:node_5675,prsc:2|A-7095-RGB,B-6931-OUT,T-5559-OUT;n:type:ShaderForge.SFN_Vector3,id:6931,x:31420,y:33481,varname:node_6931,prsc:2,v1:0,v2:0,v3:-1;n:type:ShaderForge.SFN_Slider,id:5559,x:31342,y:33611,ptovrint:False,ptlb:Normal Intensity1,ptin:_NormalIntensity1,varname:_NormalIntensity2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:8266-2496-614-7157-6035-5559-1863;pass:END;sub:END;*/

Shader "Shader Forge/SimpleLit_DistanceBlend_w_Normals" {
    Properties {
        _TextureColor1 ("Texture Color 1", 2D) = "white" {}
        _Normal1 ("Normal1", 2D) = "bump" {}
        _TextureColor2 ("Texture Color 2", 2D) = "white" {}
        _Normal2 ("Normal2", 2D) = "bump" {}
        _TransitionDistance ("Transition Distance", Float ) = 0
        _NormalIntensity1 ("Normal Intensity1", Range(0, 1)) = 0
        _NormalIntensity2 ("Normal Intensity2", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _TextureColor2; uniform float4 _TextureColor2_ST;
            uniform sampler2D _TextureColor1; uniform float4 _TextureColor1_ST;
            uniform float _TransitionDistance;
            uniform sampler2D _Normal2; uniform float4 _Normal2_ST;
            uniform sampler2D _Normal1; uniform float4 _Normal1_ST;
            uniform float _NormalIntensity2;
            uniform float _NormalIntensity1;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float3 tangentDir : TEXCOORD4;
                float3 bitangentDir : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_5188 = UnpackNormal(tex2D(_Normal2,TRANSFORM_TEX(i.uv2, _Normal2)));
                float3 _Normal_copy = UnpackNormal(tex2D(_Normal1,TRANSFORM_TEX(i.uv2, _Normal1)));
                float node_6658 = saturate((distance(i.posWorld.rgb,_WorldSpaceCameraPos)/_TransitionDistance));
                float3 normalLocal = lerp(lerp(node_5188.rgb,float3(0,0,-1),_NormalIntensity2),lerp(_Normal_copy.rgb,float3(0,0,-1),_NormalIntensity1),node_6658);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _TextureColor2_var = tex2D(_TextureColor2,TRANSFORM_TEX(i.uv0, _TextureColor2));
                float4 _TextureColor1_var = tex2D(_TextureColor1,TRANSFORM_TEX(i.uv0, _TextureColor1));
                float3 diffuseColor = lerp(_TextureColor2_var.rgb,_TextureColor1_var.rgb,node_6658);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _TextureColor2; uniform float4 _TextureColor2_ST;
            uniform sampler2D _TextureColor1; uniform float4 _TextureColor1_ST;
            uniform float _TransitionDistance;
            uniform sampler2D _Normal2; uniform float4 _Normal2_ST;
            uniform sampler2D _Normal1; uniform float4 _Normal1_ST;
            uniform float _NormalIntensity2;
            uniform float _NormalIntensity1;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                float3 tangentDir : TEXCOORD4;
                float3 bitangentDir : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_5188 = UnpackNormal(tex2D(_Normal2,TRANSFORM_TEX(i.uv2, _Normal2)));
                float3 _Normal_copy = UnpackNormal(tex2D(_Normal1,TRANSFORM_TEX(i.uv2, _Normal1)));
                float node_6658 = saturate((distance(i.posWorld.rgb,_WorldSpaceCameraPos)/_TransitionDistance));
                float3 normalLocal = lerp(lerp(node_5188.rgb,float3(0,0,-1),_NormalIntensity2),lerp(_Normal_copy.rgb,float3(0,0,-1),_NormalIntensity1),node_6658);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _TextureColor2_var = tex2D(_TextureColor2,TRANSFORM_TEX(i.uv0, _TextureColor2));
                float4 _TextureColor1_var = tex2D(_TextureColor1,TRANSFORM_TEX(i.uv0, _TextureColor1));
                float3 diffuseColor = lerp(_TextureColor2_var.rgb,_TextureColor1_var.rgb,node_6658);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
