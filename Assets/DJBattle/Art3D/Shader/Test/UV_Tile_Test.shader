// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-8379-RGB;n:type:ShaderForge.SFN_Color,id:7241,x:32471,y:32812,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_UVTile,id:5848,x:32048,y:32460,varname:node_5848,prsc:2|UVIN-6058-UVOUT,WDT-4438-OUT,HGT-4552-OUT,TILE-799-OUT;n:type:ShaderForge.SFN_TexCoord,id:6058,x:31710,y:32405,varname:node_6058,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2d,id:8379,x:32269,y:32545,varname:node_8379,prsc:2,tex:bf812b3b358a89f4eae7800a72f79457,ntxv:0,isnm:False|UVIN-5848-UVOUT,TEX-3249-TEX;n:type:ShaderForge.SFN_Vector1,id:4438,x:31710,y:32590,varname:node_4438,prsc:2,v1:8;n:type:ShaderForge.SFN_Vector1,id:4552,x:31710,y:32649,varname:node_4552,prsc:2,v1:8;n:type:ShaderForge.SFN_Vector1,id:799,x:31710,y:32706,varname:node_799,prsc:2,v1:60;n:type:ShaderForge.SFN_Tex2dAsset,id:3249,x:32048,y:32629,ptovrint:False,ptlb:node_3249,ptin:_node_3249,varname:node_3249,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bf812b3b358a89f4eae7800a72f79457,ntxv:0,isnm:False;proporder:7241-3249;pass:END;sub:END;*/

Shader "Shader Forge/UV_Tile_Test" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _node_3249 ("node_3249", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _node_3249; uniform float4 _node_3249_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float node_4438 = 8.0;
                float node_799 = 60.0;
                float2 node_5848_tc_rcp = float2(1.0,1.0)/float2( node_4438, 8.0 );
                float node_5848_ty = floor(node_799 * node_5848_tc_rcp.x);
                float node_5848_tx = node_799 - node_4438 * node_5848_ty;
                float2 node_5848 = (i.uv0 + float2(node_5848_tx, node_5848_ty)) * node_5848_tc_rcp;
                float4 node_8379 = tex2D(_node_3249,TRANSFORM_TEX(node_5848, _node_3249));
                float3 emissive = node_8379.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
