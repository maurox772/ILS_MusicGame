// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-6151-OUT;n:type:ShaderForge.SFN_Cubemap,id:9835,x:31449,y:32497,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:_Cubemap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:2a3ec67763a393546a96a22f60815b96,pvfc:0;n:type:ShaderForge.SFN_Multiply,id:8827,x:31724,y:32646,varname:node_8827,prsc:2|A-9835-RGB,B-8625-OUT,C-1836-RGB;n:type:ShaderForge.SFN_ValueProperty,id:8625,x:31449,y:32700,ptovrint:False,ptlb:Reflection Intesity,ptin:_ReflectionIntesity,varname:_ReflectionIntesity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Fresnel,id:5310,x:31136,y:33040,varname:node_5310,prsc:2|NRM-1530-OUT;n:type:ShaderForge.SFN_NormalVector,id:1530,x:30908,y:32952,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:1023,x:31449,y:32997,varname:node_1023,prsc:2|A-5310-OUT,B-2296-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2296,x:31136,y:33232,ptovrint:False,ptlb:Rim Intensity,ptin:_RimIntensity,varname:_RimIntensity,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.2;n:type:ShaderForge.SFN_Add,id:8110,x:32000,y:32918,varname:node_8110,prsc:2|A-8827-OUT,B-3239-OUT;n:type:ShaderForge.SFN_Multiply,id:3239,x:31724,y:32924,varname:node_3239,prsc:2|A-1836-RGB,B-1023-OUT;n:type:ShaderForge.SFN_Add,id:5430,x:32346,y:32810,varname:node_5430,prsc:2|A-7331-OUT,B-8110-OUT,C-2028-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:4148,x:31135,y:32603,ptovrint:False,ptlb:Texture Color,ptin:_TextureColor,varname:node_4148,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:49281c8b996cbb543adb1d2f711d464e,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1836,x:31449,y:32803,varname:node_1836,prsc:2,tex:49281c8b996cbb543adb1d2f711d464e,ntxv:0,isnm:False|TEX-4148-TEX;n:type:ShaderForge.SFN_Multiply,id:2028,x:32000,y:33226,varname:node_2028,prsc:2|A-549-RGB,B-608-RGB;n:type:ShaderForge.SFN_Color,id:608,x:31731,y:33485,ptovrint:False,ptlb:Mask Color,ptin:_MaskColor,varname:node_608,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2dAsset,id:4766,x:31449,y:33441,ptovrint:False,ptlb:Texture Mask,ptin:_TextureMask,varname:node_4766,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:beb0c4b4d171555439f7da81063ae4cc,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:549,x:31731,y:33297,varname:node_549,prsc:2,tex:beb0c4b4d171555439f7da81063ae4cc,ntxv:0,isnm:False|TEX-4766-TEX;n:type:ShaderForge.SFN_Tex2d,id:2772,x:31449,y:32338,varname:node_2772,prsc:2,tex:49281c8b996cbb543adb1d2f711d464e,ntxv:0,isnm:False|TEX-4148-TEX;n:type:ShaderForge.SFN_Multiply,id:7331,x:31987,y:32450,varname:node_7331,prsc:2|A-4045-OUT,B-2772-RGB;n:type:ShaderForge.SFN_ValueProperty,id:4045,x:31711,y:32292,ptovrint:False,ptlb:Color Intensity,ptin:_ColorIntensity,varname:_ReflectionIntesity_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Blend,id:6151,x:32531,y:32888,varname:node_6151,prsc:2,blmd:10,clmp:True|SRC-5430-OUT,DST-4087-RGB;n:type:ShaderForge.SFN_Color,id:4087,x:32346,y:32986,ptovrint:False,ptlb:MainColor,ptin:_MainColor,varname:node_4087,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5073529,c2:0.5073529,c3:0.5019608,c4:1;proporder:4087-4045-4148-9835-8625-2296-608-4766;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_Rim_n_Cubemap" {
    Properties {
        _MainColor ("MainColor", Color) = (0.5073529,0.5073529,0.5019608,1)
        _ColorIntensity ("Color Intensity", Float ) = 1
        _TextureColor ("Texture Color", 2D) = "white" {}
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _ReflectionIntesity ("Reflection Intesity", Float ) = 1
        _RimIntensity ("Rim Intensity", Float ) = 1.2
        _MaskColor ("Mask Color", Color) = (1,1,1,1)
        _TextureMask ("Texture Mask", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform samplerCUBE _Cubemap;
            uniform float _ReflectionIntesity;
            uniform float _RimIntensity;
            uniform sampler2D _TextureColor; uniform float4 _TextureColor_ST;
            uniform float4 _MaskColor;
            uniform sampler2D _TextureMask; uniform float4 _TextureMask_ST;
            uniform float _ColorIntensity;
            uniform float4 _MainColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float4 node_2772 = tex2D(_TextureColor,TRANSFORM_TEX(i.uv0, _TextureColor));
                float4 node_1836 = tex2D(_TextureColor,TRANSFORM_TEX(i.uv0, _TextureColor));
                float4 node_549 = tex2D(_TextureMask,TRANSFORM_TEX(i.uv0, _TextureMask));
                float3 node_5430 = ((_ColorIntensity*node_2772.rgb)+((texCUBE(_Cubemap,viewReflectDirection).rgb*_ReflectionIntesity*node_1836.rgb)+(node_1836.rgb*((1.0-max(0,dot(i.normalDir, viewDirection)))*_RimIntensity)))+(node_549.rgb*_MaskColor.rgb));
                float3 emissive = saturate(( _MainColor.rgb > 0.5 ? (1.0-(1.0-2.0*(_MainColor.rgb-0.5))*(1.0-node_5430)) : (2.0*_MainColor.rgb*node_5430) ));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
