// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-903-OUT,alpha-4813-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:31732,y:32946,ptovrint:False,ptlb:Color 2,ptin:_Color2,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Cubemap,id:9835,x:31937,y:32357,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_9835,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:2a3ec67763a393546a96a22f60815b96,pvfc:0;n:type:ShaderForge.SFN_Color,id:4240,x:31732,y:32769,ptovrint:False,ptlb:Color 1,ptin:_Color1,varname:node_4240,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:8827,x:32224,y:32603,varname:node_8827,prsc:2|A-9835-RGB,B-8625-OUT,C-4591-RGB;n:type:ShaderForge.SFN_Blend,id:903,x:32436,y:32793,varname:node_903,prsc:2,blmd:5,clmp:True|SRC-8827-OUT,DST-7452-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8625,x:31937,y:32526,ptovrint:False,ptlb:Reflection,ptin:_Reflection,varname:node_8625,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Lerp,id:7452,x:32053,y:32847,varname:node_7452,prsc:2|A-4240-RGB,B-7241-RGB,T-1023-OUT;n:type:ShaderForge.SFN_Fresnel,id:5310,x:31732,y:33197,varname:node_5310,prsc:2|NRM-1530-OUT;n:type:ShaderForge.SFN_NormalVector,id:1530,x:31504,y:33109,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:1023,x:32045,y:33154,varname:node_1023,prsc:2|A-5310-OUT,B-2296-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2296,x:31732,y:33389,ptovrint:False,ptlb:Rim Intensity,ptin:_RimIntensity,varname:node_2296,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.2;n:type:ShaderForge.SFN_ValueProperty,id:4813,x:32310,y:33134,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_4813,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Color,id:4591,x:31937,y:32621,ptovrint:False,ptlb:Reflection Color,ptin:_ReflectionColor,varname:node_4591,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:4240-7241-9835-4591-8625-2296-4813;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_Glass" {
    Properties {
        _Color1 ("Color 1", Color) = (0.5,0.5,0.5,1)
        _Color2 ("Color 2", Color) = (0.07843138,0.3921569,0.7843137,1)
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _ReflectionColor ("Reflection Color", Color) = (0.5,0.5,0.5,1)
        _Reflection ("Reflection", Float ) = 0
        _RimIntensity ("Rim Intensity", Float ) = 1.2
        _Alpha ("Alpha", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color2;
            uniform samplerCUBE _Cubemap;
            uniform float4 _Color1;
            uniform float _Reflection;
            uniform float _RimIntensity;
            uniform float _Alpha;
            uniform float4 _ReflectionColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float3 emissive = saturate(max((texCUBE(_Cubemap,viewReflectDirection).rgb*_Reflection*_ReflectionColor.rgb),lerp(_Color1.rgb,_Color2.rgb,((1.0-max(0,dot(i.normalDir, viewDirection)))*_RimIntensity))));
                float3 finalColor = emissive;
                return fixed4(finalColor,_Alpha);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
