// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:36048,y:33514,varname:node_3138,prsc:2|emission-4814-OUT,clip-3997-OUT;n:type:ShaderForge.SFN_Tex2d,id:7677,x:32606,y:32631,varname:node_7677,prsc:2,tex:70dd85c2940616742a36a6b708c7b614,ntxv:0,isnm:False|UVIN-8757-OUT,TEX-4576-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:4576,x:30990,y:33200,ptovrint:False,ptlb:Main Texture,ptin:_MainTexture,varname:node_4576,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:70dd85c2940616742a36a6b708c7b614,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2070,x:31763,y:32741,varname:node_2070,prsc:2|A-9955-OUT,B-9079-OUT;n:type:ShaderForge.SFN_Time,id:6179,x:30990,y:33426,varname:node_6179,prsc:2;n:type:ShaderForge.SFN_Slider,id:9079,x:31388,y:32915,ptovrint:False,ptlb:Main Speed,ptin:_MainSpeed,varname:node_9079,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:1,max:10;n:type:ShaderForge.SFN_Add,id:4019,x:31998,y:33183,varname:node_4019,prsc:2|A-2070-OUT,B-8519-V;n:type:ShaderForge.SFN_TexCoord,id:8519,x:31776,y:33207,varname:node_8519,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Append,id:7330,x:32185,y:33226,varname:node_7330,prsc:2|A-8519-U,B-4019-OUT;n:type:ShaderForge.SFN_Add,id:8279,x:33255,y:33577,varname:node_8279,prsc:2|A-7677-R,B-2484-R;n:type:ShaderForge.SFN_Tex2d,id:5997,x:32606,y:32852,varname:node_5997,prsc:2,tex:70dd85c2940616742a36a6b708c7b614,ntxv:0,isnm:False|TEX-4576-TEX;n:type:ShaderForge.SFN_Clamp01,id:9701,x:33895,y:33761,varname:node_9701,prsc:2|IN-6085-OUT;n:type:ShaderForge.SFN_Multiply,id:307,x:32002,y:33396,varname:node_307,prsc:2|A-6179-TSL,B-5852-OUT;n:type:ShaderForge.SFN_Slider,id:5852,x:31561,y:33539,ptovrint:False,ptlb:Distort Speed,ptin:_DistortSpeed,varname:node_5852,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:-4,max:10;n:type:ShaderForge.SFN_Append,id:8606,x:32171,y:33689,varname:node_8606,prsc:2|A-36-U,B-6583-OUT;n:type:ShaderForge.SFN_Tex2d,id:4036,x:32391,y:33689,varname:node_4036,prsc:2,tex:70dd85c2940616742a36a6b708c7b614,ntxv:0,isnm:False|UVIN-8606-OUT,TEX-4576-TEX;n:type:ShaderForge.SFN_TexCoord,id:36,x:31834,y:33677,varname:node_36,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Add,id:6583,x:32002,y:33571,varname:node_6583,prsc:2|A-307-OUT,B-36-V;n:type:ShaderForge.SFN_Multiply,id:5264,x:32816,y:33689,varname:node_5264,prsc:2|A-1044-OUT,B-5454-OUT;n:type:ShaderForge.SFN_RemapRange,id:5454,x:32604,y:33689,varname:node_5454,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-4036-G;n:type:ShaderForge.SFN_Slider,id:1044,x:32447,y:33593,ptovrint:False,ptlb:Distort Amount,ptin:_DistortAmount,varname:node_1044,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.025,max:1;n:type:ShaderForge.SFN_Add,id:8757,x:32605,y:33241,varname:node_8757,prsc:2|A-7330-OUT,B-5264-OUT;n:type:ShaderForge.SFN_TexCoord,id:1870,x:31752,y:34375,varname:node_1870,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Append,id:6391,x:32213,y:34391,varname:node_6391,prsc:2|A-1870-U,B-7187-OUT;n:type:ShaderForge.SFN_Add,id:7187,x:32000,y:34270,varname:node_7187,prsc:2|A-3950-OUT,B-1870-V;n:type:ShaderForge.SFN_Add,id:7550,x:32575,y:34393,varname:node_7550,prsc:2|A-6391-OUT,B-8757-OUT;n:type:ShaderForge.SFN_Tex2d,id:2484,x:32572,y:34133,varname:node_2484,prsc:2,tex:70dd85c2940616742a36a6b708c7b614,ntxv:0,isnm:False|UVIN-7550-OUT,TEX-4576-TEX;n:type:ShaderForge.SFN_Slider,id:9786,x:31593,y:33917,ptovrint:False,ptlb:Secondary Speed,ptin:_SecondarySpeed,varname:node_9786,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:0.25,max:10;n:type:ShaderForge.SFN_Multiply,id:3950,x:32003,y:33866,varname:node_3950,prsc:2|A-6179-TSL,B-9786-OUT;n:type:ShaderForge.SFN_Sin,id:227,x:30995,y:32760,varname:node_227,prsc:2|IN-6179-TSL;n:type:ShaderForge.SFN_TexCoord,id:1043,x:33067,y:33028,varname:node_1043,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_RemapRange,id:1464,x:33277,y:33028,varname:node_1464,prsc:2,frmn:0,frmx:1,tomn:-0.75,tomx:0.75|IN-1043-U;n:type:ShaderForge.SFN_Abs,id:7414,x:33458,y:33028,varname:node_7414,prsc:2|IN-1464-OUT;n:type:ShaderForge.SFN_RemapRange,id:311,x:33628,y:33028,varname:node_311,prsc:2,frmn:0,frmx:1,tomn:0,tomx:0.35|IN-7414-OUT;n:type:ShaderForge.SFN_Clamp01,id:376,x:33801,y:33028,varname:node_376,prsc:2|IN-311-OUT;n:type:ShaderForge.SFN_OneMinus,id:5076,x:33984,y:33028,varname:node_5076,prsc:2|IN-376-OUT;n:type:ShaderForge.SFN_Multiply,id:1895,x:33255,y:33293,varname:node_1895,prsc:2|A-5076-OUT,B-8279-OUT;n:type:ShaderForge.SFN_Add,id:6085,x:33526,y:33761,varname:node_6085,prsc:2|A-1895-OUT,B-5997-B;n:type:ShaderForge.SFN_Color,id:7881,x:33796,y:33279,ptovrint:False,ptlb:Color 1,ptin:_Color1,varname:node_7881,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.9172413,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:4782,x:33796,y:33481,ptovrint:False,ptlb:Color 2,ptin:_Color2,varname:_Color2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1795523,c2:0.3345223,c3:0.9044118,c4:1;n:type:ShaderForge.SFN_Lerp,id:2579,x:34105,y:33561,varname:node_2579,prsc:2|A-7881-RGB,B-4782-RGB,T-6085-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:3577,x:34118,y:34002,varname:node_3577,prsc:2|IN-9701-OUT,IMIN-9436-OUT,IMAX-5889-OUT,OMIN-2713-OUT,OMAX-2815-OUT;n:type:ShaderForge.SFN_Vector1,id:9436,x:33862,y:34066,varname:node_9436,prsc:2,v1:-0.1;n:type:ShaderForge.SFN_Vector1,id:5889,x:33862,y:34132,varname:node_5889,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Vector1,id:2713,x:33862,y:34200,varname:node_2713,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Vector1,id:2815,x:33862,y:34275,varname:node_2815,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:3997,x:34379,y:34069,varname:node_3997,prsc:2|A-3577-OUT,B-1985-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1985,x:34153,y:34221,ptovrint:False,ptlb:Alpha Cutout,ptin:_AlphaCutout,varname:node_1985,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Fresnel,id:838,x:34737,y:33141,varname:node_838,prsc:2|NRM-6320-OUT;n:type:ShaderForge.SFN_NormalVector,id:6320,x:34472,y:33143,prsc:2,pt:False;n:type:ShaderForge.SFN_OneMinus,id:5979,x:34737,y:33346,varname:node_5979,prsc:2|IN-838-OUT;n:type:ShaderForge.SFN_Add,id:4814,x:35836,y:33611,varname:node_4814,prsc:2|A-4784-OUT,B-1152-OUT,C-2579-OUT;n:type:ShaderForge.SFN_Color,id:4334,x:35113,y:32839,ptovrint:False,ptlb:Outer Rim Color,ptin:_OuterRimColor,varname:node_4334,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:4784,x:35443,y:32939,varname:node_4784,prsc:2|A-4334-RGB,B-838-OUT,C-9770-OUT,D-8910-OUT;n:type:ShaderForge.SFN_Slider,id:9770,x:35313,y:33134,ptovrint:False,ptlb:Outer Rim Intesity,ptin:_OuterRimIntesity,varname:node_9770,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.5,max:5;n:type:ShaderForge.SFN_Color,id:6405,x:35107,y:33200,ptovrint:False,ptlb:Inner Rim Color,ptin:_InnerRimColor,varname:_OuterRimColor_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0.006896496,c4:1;n:type:ShaderForge.SFN_Multiply,id:1152,x:35436,y:33341,varname:node_1152,prsc:2|A-6405-RGB,B-5979-OUT,C-699-OUT,D-9900-OUT;n:type:ShaderForge.SFN_Slider,id:699,x:35314,y:33519,ptovrint:False,ptlb:Inner Rim Intensity,ptin:_InnerRimIntensity,varname:_OuterRimIntesity_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4,max:5;n:type:ShaderForge.SFN_ToggleProperty,id:8910,x:34737,y:33046,ptovrint:False,ptlb:Outer Rim,ptin:_OuterRim,varname:node_8910,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_ToggleProperty,id:9900,x:34737,y:33502,ptovrint:False,ptlb:Inner Rim,ptin:_InnerRim,varname:_RimToggle_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_SwitchProperty,id:9955,x:31537,y:32592,ptovrint:False,ptlb:Ping Pong,ptin:_PingPong,varname:node_9955,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-6179-TSL,B-227-OUT;proporder:4576-7881-4782-9955-9079-9786-1044-5852-1985-8910-4334-9770-9900-6405-699;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_LavaLamp" {
    Properties {
        _MainTexture ("Main Texture", 2D) = "white" {}
        _Color1 ("Color 1", Color) = (0,0.9172413,1,1)
        _Color2 ("Color 2", Color) = (0.1795523,0.3345223,0.9044118,1)
        [MaterialToggle] _PingPong ("Ping Pong", Float ) = 0
        _MainSpeed ("Main Speed", Range(-10, 10)) = 1
        _SecondarySpeed ("Secondary Speed", Range(-10, 10)) = 0.25
        _DistortAmount ("Distort Amount", Range(0, 1)) = 0.025
        _DistortSpeed ("Distort Speed", Range(-10, 10)) = -4
        _AlphaCutout ("Alpha Cutout", Float ) = 1
        [MaterialToggle] _OuterRim ("Outer Rim", Float ) = 1
        _OuterRimColor ("Outer Rim Color", Color) = (1,0,0,1)
        _OuterRimIntesity ("Outer Rim Intesity", Range(0, 5)) = 1.5
        [MaterialToggle] _InnerRim ("Inner Rim", Float ) = 1
        _InnerRimColor ("Inner Rim Color", Color) = (0,1,0.006896496,1)
        _InnerRimIntensity ("Inner Rim Intensity", Range(0, 5)) = 0.4
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTexture; uniform float4 _MainTexture_ST;
            uniform float _MainSpeed;
            uniform float _DistortSpeed;
            uniform float _DistortAmount;
            uniform float _SecondarySpeed;
            uniform float4 _Color1;
            uniform float4 _Color2;
            uniform float _AlphaCutout;
            uniform float4 _OuterRimColor;
            uniform float _OuterRimIntesity;
            uniform float4 _InnerRimColor;
            uniform float _InnerRimIntensity;
            uniform fixed _OuterRim;
            uniform fixed _InnerRim;
            uniform fixed _PingPong;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 node_6179 = _Time + _TimeEditor;
                float2 node_8606 = float2(i.uv0.r,((node_6179.r*_DistortSpeed)+i.uv0.g));
                float4 node_4036 = tex2D(_MainTexture,TRANSFORM_TEX(node_8606, _MainTexture));
                float2 node_8757 = (float2(i.uv0.r,((lerp( node_6179.r, sin(node_6179.r), _PingPong )*_MainSpeed)+i.uv0.g))+(_DistortAmount*(node_4036.g*2.0+-1.0)));
                float4 node_7677 = tex2D(_MainTexture,TRANSFORM_TEX(node_8757, _MainTexture));
                float2 node_7550 = (float2(i.uv0.r,((node_6179.r*_SecondarySpeed)+i.uv0.g))+node_8757);
                float4 node_2484 = tex2D(_MainTexture,TRANSFORM_TEX(node_7550, _MainTexture));
                float4 node_5997 = tex2D(_MainTexture,TRANSFORM_TEX(i.uv0, _MainTexture));
                float node_6085 = (((1.0 - saturate((abs((i.uv0.r*1.5+-0.75))*0.35+0.0)))*(node_7677.r+node_2484.r))+node_5997.b);
                float node_9436 = (-0.1);
                float node_2713 = 0.1;
                clip(((node_2713 + ( (saturate(node_6085) - node_9436) * (1.0 - node_2713) ) / (0.5 - node_9436))*_AlphaCutout) - 0.5);
////// Lighting:
////// Emissive:
                float node_838 = (1.0-max(0,dot(i.normalDir, viewDirection)));
                float3 emissive = ((_OuterRimColor.rgb*node_838*_OuterRimIntesity*_OuterRim)+(_InnerRimColor.rgb*(1.0 - node_838)*_InnerRimIntensity*_InnerRim)+lerp(_Color1.rgb,_Color2.rgb,node_6085));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTexture; uniform float4 _MainTexture_ST;
            uniform float _MainSpeed;
            uniform float _DistortSpeed;
            uniform float _DistortAmount;
            uniform float _SecondarySpeed;
            uniform float _AlphaCutout;
            uniform fixed _PingPong;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 node_6179 = _Time + _TimeEditor;
                float2 node_8606 = float2(i.uv0.r,((node_6179.r*_DistortSpeed)+i.uv0.g));
                float4 node_4036 = tex2D(_MainTexture,TRANSFORM_TEX(node_8606, _MainTexture));
                float2 node_8757 = (float2(i.uv0.r,((lerp( node_6179.r, sin(node_6179.r), _PingPong )*_MainSpeed)+i.uv0.g))+(_DistortAmount*(node_4036.g*2.0+-1.0)));
                float4 node_7677 = tex2D(_MainTexture,TRANSFORM_TEX(node_8757, _MainTexture));
                float2 node_7550 = (float2(i.uv0.r,((node_6179.r*_SecondarySpeed)+i.uv0.g))+node_8757);
                float4 node_2484 = tex2D(_MainTexture,TRANSFORM_TEX(node_7550, _MainTexture));
                float4 node_5997 = tex2D(_MainTexture,TRANSFORM_TEX(i.uv0, _MainTexture));
                float node_6085 = (((1.0 - saturate((abs((i.uv0.r*1.5+-0.75))*0.35+0.0)))*(node_7677.r+node_2484.r))+node_5997.b);
                float node_9436 = (-0.1);
                float node_2713 = 0.1;
                clip(((node_2713 + ( (saturate(node_6085) - node_9436) * (1.0 - node_2713) ) / (0.5 - node_9436))*_AlphaCutout) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
