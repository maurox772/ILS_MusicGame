// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-633-RGB,clip-633-A;n:type:ShaderForge.SFN_UVTile,id:6871,x:32178,y:32868,varname:node_6871,prsc:2|UVIN-1479-UVOUT,WDT-2724-OUT,HGT-7273-OUT,TILE-8433-OUT;n:type:ShaderForge.SFN_Tex2d,id:633,x:32399,y:32868,ptovrint:False,ptlb:Face,ptin:_Face,varname:node_633,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e1ab2d97d13b9704e83e45cd5a7c9ec0,ntxv:0,isnm:False|UVIN-6871-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:1479,x:31841,y:32554,varname:node_1479,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Time,id:7895,x:31344,y:32951,varname:node_7895,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6274,x:31628,y:33043,varname:node_6274,prsc:2|A-7895-T,B-7863-OUT,C-5893-OUT;n:type:ShaderForge.SFN_Round,id:8433,x:31840,y:33043,varname:node_8433,prsc:2|IN-6274-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7863,x:31344,y:33187,ptovrint:False,ptlb:Animation Speed,ptin:_AnimationSpeed,varname:node_7863,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_ToggleProperty,id:5893,x:31344,y:33301,ptovrint:False,ptlb:isActive,ptin:_isActive,varname:node_5893,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True;n:type:ShaderForge.SFN_ValueProperty,id:2724,x:31841,y:32770,ptovrint:False,ptlb:Col,ptin:_Col,varname:node_2724,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:8;n:type:ShaderForge.SFN_ValueProperty,id:7273,x:31841,y:32915,ptovrint:False,ptlb:Row,ptin:_Row,varname:node_7273,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;proporder:633-2724-7273-7863-5893;pass:END;sub:END;*/

Shader "IndieLevel/Unlit_Toon_Faces_1" {
    Properties {
        _Face ("Face", 2D) = "white" {}
        _Col ("Col", Float ) = 8
        _Row ("Row", Float ) = 1
        _AnimationSpeed ("Animation Speed", Float ) = 2
        [MaterialToggle] _isActive ("isActive", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _Face; uniform float4 _Face_ST;
            uniform float _AnimationSpeed;
            uniform fixed _isActive;
            uniform float _Col;
            uniform float _Row;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_7895 = _Time + _TimeEditor;
                float node_8433 = round((node_7895.g*_AnimationSpeed*_isActive));
                float2 node_6871_tc_rcp = float2(1.0,1.0)/float2( _Col, _Row );
                float node_6871_ty = floor(node_8433 * node_6871_tc_rcp.x);
                float node_6871_tx = node_8433 - _Col * node_6871_ty;
                float2 node_6871 = (i.uv0 + float2(node_6871_tx, node_6871_ty)) * node_6871_tc_rcp;
                float4 _Face_var = tex2D(_Face,TRANSFORM_TEX(node_6871, _Face));
                clip(_Face_var.a - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = _Face_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _Face; uniform float4 _Face_ST;
            uniform float _AnimationSpeed;
            uniform fixed _isActive;
            uniform float _Col;
            uniform float _Row;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_7895 = _Time + _TimeEditor;
                float node_8433 = round((node_7895.g*_AnimationSpeed*_isActive));
                float2 node_6871_tc_rcp = float2(1.0,1.0)/float2( _Col, _Row );
                float node_6871_ty = floor(node_8433 * node_6871_tc_rcp.x);
                float node_6871_tx = node_8433 - _Col * node_6871_ty;
                float2 node_6871 = (i.uv0 + float2(node_6871_tx, node_6871_ty)) * node_6871_tc_rcp;
                float4 _Face_var = tex2D(_Face,TRANSFORM_TEX(node_6871, _Face));
                clip(_Face_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
