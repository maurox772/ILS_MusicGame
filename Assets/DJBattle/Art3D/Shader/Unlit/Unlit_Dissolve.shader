// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-720-RGB,alpha-2395-OUT;n:type:ShaderForge.SFN_Slider,id:54,x:30235,y:32548,ptovrint:False,ptlb:Dissolve Amount,ptin:_DissolveAmount,varname:node_54,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_OneMinus,id:5446,x:30665,y:32415,varname:node_5446,prsc:2|IN-54-OUT;n:type:ShaderForge.SFN_RemapRange,id:5208,x:30864,y:32436,varname:node_5208,prsc:2,frmn:0,frmx:1,tomn:-0.6,tomx:0.6|IN-5446-OUT;n:type:ShaderForge.SFN_Tex2d,id:7348,x:30864,y:32647,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:node_7348,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:71db79218fa8ae14ea20864e78b132b1,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:2395,x:31142,y:32569,varname:node_2395,prsc:2|A-5208-OUT,B-7348-R;n:type:ShaderForge.SFN_RemapRange,id:3381,x:31380,y:32454,varname:node_3381,prsc:2,frmn:0,frmx:1,tomn:-4,tomx:4|IN-2395-OUT;n:type:ShaderForge.SFN_Clamp01,id:6504,x:31595,y:32454,varname:node_6504,prsc:2|IN-3381-OUT;n:type:ShaderForge.SFN_OneMinus,id:8286,x:31770,y:32454,varname:node_8286,prsc:2|IN-6504-OUT;n:type:ShaderForge.SFN_Append,id:1734,x:31990,y:32492,varname:node_1734,prsc:2|A-8286-OUT,B-6061-OUT;n:type:ShaderForge.SFN_Vector1,id:6061,x:31815,y:32631,varname:node_6061,prsc:2,v1:0;n:type:ShaderForge.SFN_Tex2dAsset,id:528,x:32144,y:32768,ptovrint:False,ptlb:Ramp,ptin:_Ramp,varname:node_528,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:432e912e3363c0d418a6d55b2df7f95a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:720,x:32355,y:32593,varname:node_720,prsc:2,tex:432e912e3363c0d418a6d55b2df7f95a,ntxv:0,isnm:False|UVIN-1734-OUT,TEX-528-TEX;proporder:54-7348-528;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_Dissolve" {
    Properties {
        _DissolveAmount ("Dissolve Amount", Range(0, 1)) = 0
        _Noise ("Noise", 2D) = "white" {}
        _Ramp ("Ramp", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _DissolveAmount;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform sampler2D _Ramp; uniform float4 _Ramp_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                float node_2395 = (((1.0 - _DissolveAmount)*1.2+-0.6)+_Noise_var.r);
                float2 node_1734 = float2((1.0 - saturate((node_2395*8.0+-4.0))),0.0);
                float4 node_720 = tex2D(_Ramp,TRANSFORM_TEX(node_1734, _Ramp));
                float3 emissive = node_720.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,node_2395);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
