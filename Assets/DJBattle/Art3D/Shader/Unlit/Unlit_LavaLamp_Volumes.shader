// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-5946-OUT;n:type:ShaderForge.SFN_Fresnel,id:1056,x:30992,y:32579,varname:node_1056,prsc:2|NRM-8768-OUT;n:type:ShaderForge.SFN_NormalVector,id:8768,x:30812,y:32569,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:9603,x:32192,y:32698,varname:node_9603,prsc:2|A-3810-OUT,B-2591-RGB,C-1056-OUT,D-6034-OUT;n:type:ShaderForge.SFN_Slider,id:3810,x:31829,y:32419,ptovrint:False,ptlb:Outer Rim Intensity,ptin:_OuterRimIntensity,varname:node_3810,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2,max:10;n:type:ShaderForge.SFN_OneMinus,id:4045,x:31161,y:32649,varname:node_4045,prsc:2|IN-1056-OUT;n:type:ShaderForge.SFN_Slider,id:5950,x:31810,y:33189,ptovrint:False,ptlb:Inner Rim Intensity,ptin:_InnerRimIntensity,varname:_node_3810_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:10;n:type:ShaderForge.SFN_Multiply,id:4458,x:32192,y:32865,varname:node_4458,prsc:2|A-2553-OUT,B-6034-OUT,C-8030-RGB,D-5950-OUT;n:type:ShaderForge.SFN_Add,id:5946,x:32402,y:32810,varname:node_5946,prsc:2|A-1148-RGB,B-9603-OUT,C-4458-OUT;n:type:ShaderForge.SFN_RemapRange,id:2553,x:31338,y:32696,varname:node_2553,prsc:2,frmn:0,frmx:1,tomn:0,tomx:5|IN-4045-OUT;n:type:ShaderForge.SFN_Color,id:1148,x:32192,y:32513,ptovrint:False,ptlb:Base Color,ptin:_BaseColor,varname:node_1148,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Time,id:3551,x:30528,y:32847,varname:node_3551,prsc:2;n:type:ShaderForge.SFN_Sin,id:1977,x:30986,y:32888,varname:node_1977,prsc:2|IN-2833-OUT;n:type:ShaderForge.SFN_Multiply,id:2833,x:30816,y:32888,varname:node_2833,prsc:2|A-3551-T,B-4610-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4610,x:30528,y:33029,ptovrint:False,ptlb:Rim Flickering Speed,ptin:_RimFlickeringSpeed,varname:node_4610,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.35;n:type:ShaderForge.SFN_Abs,id:7071,x:31161,y:32888,varname:node_7071,prsc:2|IN-1977-OUT;n:type:ShaderForge.SFN_RemapRange,id:6034,x:31338,y:32888,varname:node_6034,prsc:2,frmn:0,frmx:1,tomn:0.5,tomx:1|IN-7071-OUT;n:type:ShaderForge.SFN_TexCoord,id:8734,x:31734,y:32512,varname:node_8734,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2dAsset,id:156,x:31532,y:33030,ptovrint:False,ptlb:Colors,ptin:_Colors,varname:node_156,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bf812b3b358a89f4eae7800a72f79457,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:2591,x:31972,y:32610,varname:node_2591,prsc:2,tex:bf812b3b358a89f4eae7800a72f79457,ntxv:0,isnm:False|UVIN-8734-UVOUT,TEX-156-TEX;n:type:ShaderForge.SFN_Tex2d,id:8030,x:31967,y:33014,varname:node_8030,prsc:2,tex:bf812b3b358a89f4eae7800a72f79457,ntxv:0,isnm:False|TEX-156-TEX;proporder:156-1148-3810-5950-4610;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_LavaLamp_Volumes" {
    Properties {
        _Colors ("Colors", 2D) = "white" {}
        _BaseColor ("Base Color", Color) = (0,0,0,1)
        _OuterRimIntensity ("Outer Rim Intensity", Range(0, 10)) = 2
        _InnerRimIntensity ("Inner Rim Intensity", Range(0, 10)) = 0.5
        _RimFlickeringSpeed ("Rim Flickering Speed", Float ) = 0.35
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _OuterRimIntensity;
            uniform float _InnerRimIntensity;
            uniform float4 _BaseColor;
            uniform float _RimFlickeringSpeed;
            uniform sampler2D _Colors; uniform float4 _Colors_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_2591 = tex2D(_Colors,TRANSFORM_TEX(i.uv0, _Colors));
                float node_1056 = (1.0-max(0,dot(i.normalDir, viewDirection)));
                float4 node_3551 = _Time + _TimeEditor;
                float node_6034 = (abs(sin((node_3551.g*_RimFlickeringSpeed)))*0.5+0.5);
                float4 node_8030 = tex2D(_Colors,TRANSFORM_TEX(i.uv0, _Colors));
                float3 emissive = (_BaseColor.rgb+(_OuterRimIntensity*node_2591.rgb*node_1056*node_6034)+(((1.0 - node_1056)*5.0+0.0)*node_6034*node_8030.rgb*_InnerRimIntensity));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
