// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32646,y:32363,varname:node_3138,prsc:2|emission-9469-OUT;n:type:ShaderForge.SFN_Fresnel,id:8823,x:31091,y:32459,varname:node_8823,prsc:2|NRM-4805-OUT,EXP-147-OUT;n:type:ShaderForge.SFN_NormalVector,id:4805,x:30707,y:32351,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:999,x:31549,y:32519,varname:node_999,prsc:2|A-8823-OUT,B-237-OUT,C-6144-RGB;n:type:ShaderForge.SFN_Slider,id:237,x:31094,y:32703,ptovrint:False,ptlb:Outer Rim Intensity,ptin:_OuterRimIntensity,varname:node_237,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:5;n:type:ShaderForge.SFN_Slider,id:7059,x:31203,y:32185,ptovrint:False,ptlb:Inner Rim Intensity,ptin:_InnerRimIntensity,varname:_OuterRimIntensity_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:5;n:type:ShaderForge.SFN_OneMinus,id:6333,x:31360,y:32327,varname:node_6333,prsc:2|IN-8823-OUT;n:type:ShaderForge.SFN_Multiply,id:6309,x:31550,y:32253,varname:node_6309,prsc:2|A-4551-RGB,B-7059-OUT,C-6333-OUT;n:type:ShaderForge.SFN_Add,id:9435,x:31831,y:32408,cmnt:Final Rim,varname:node_9435,prsc:2|A-6309-OUT,B-999-OUT;n:type:ShaderForge.SFN_Color,id:6144,x:31251,y:32847,ptovrint:False,ptlb:Outer Rim Color,ptin:_OuterRimColor,varname:node_6144,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.8823529,c3:0.7058823,c4:1;n:type:ShaderForge.SFN_Color,id:4551,x:31360,y:32005,ptovrint:False,ptlb:Inner Rim Color,ptin:_InnerRimColor,varname:node_4551,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.6985294,c3:0.6985294,c4:1;n:type:ShaderForge.SFN_Tex2d,id:2598,x:31831,y:32147,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_2598,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:f7a047af24e292945b769699350413d8,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:6129,x:32128,y:32283,varname:node_6129,prsc:2|A-9433-OUT,B-9435-OUT;n:type:ShaderForge.SFN_Slider,id:8716,x:30110,y:32563,ptovrint:False,ptlb:Rim Coverage,ptin:_RimCoverage,varname:node_8716,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_OneMinus,id:406,x:30467,y:32541,varname:node_406,prsc:2|IN-8716-OUT;n:type:ShaderForge.SFN_RemapRange,id:147,x:30707,y:32669,varname:node_147,prsc:2,frmn:0,frmx:1,tomn:0,tomx:5|IN-406-OUT;n:type:ShaderForge.SFN_Power,id:9469,x:32387,y:32459,varname:node_9469,prsc:2|VAL-6129-OUT,EXP-5303-OUT;n:type:ShaderForge.SFN_Vector1,id:5303,x:32155,y:32540,varname:node_5303,prsc:2,v1:1.2;n:type:ShaderForge.SFN_Color,id:6384,x:31831,y:31961,ptovrint:False,ptlb:Main Color,ptin:_MainColor,varname:node_6384,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:9433,x:32059,y:32100,varname:node_9433,prsc:2|A-6384-RGB,B-2598-RGB;proporder:6384-2598-6144-237-4551-7059-8716;pass:END;sub:END;*/

Shader "IndieLevel/Unlit_Toon_1" {
    Properties {
        _MainColor ("Main Color", Color) = (1,1,1,1)
        _Diffuse ("Diffuse", 2D) = "white" {}
        _OuterRimColor ("Outer Rim Color", Color) = (1,0.8823529,0.7058823,1)
        _OuterRimIntensity ("Outer Rim Intensity", Range(0, 5)) = 1
        _InnerRimColor ("Inner Rim Color", Color) = (1,0.6985294,0.6985294,1)
        _InnerRimIntensity ("Inner Rim Intensity", Range(0, 5)) = 0
        _RimCoverage ("Rim Coverage", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _OuterRimIntensity;
            uniform float _InnerRimIntensity;
            uniform float4 _OuterRimColor;
            uniform float4 _InnerRimColor;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float _RimCoverage;
            uniform float4 _MainColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float node_8823 = pow(1.0-max(0,dot(i.normalDir, viewDirection)),((1.0 - _RimCoverage)*5.0+0.0));
                float3 emissive = pow(((_MainColor.rgb*_Diffuse_var.rgb)+((_InnerRimColor.rgb*_InnerRimIntensity*(1.0 - node_8823))+(node_8823*_OuterRimIntensity*_OuterRimColor.rgb))),1.2);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
