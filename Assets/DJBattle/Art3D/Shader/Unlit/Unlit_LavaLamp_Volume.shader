// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-5946-OUT;n:type:ShaderForge.SFN_Fresnel,id:1056,x:31970,y:32776,varname:node_1056,prsc:2|NRM-8768-OUT;n:type:ShaderForge.SFN_NormalVector,id:8768,x:31734,y:32766,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:9603,x:32192,y:32698,varname:node_9603,prsc:2|A-5816-RGB,B-3810-OUT,C-1056-OUT,D-6034-OUT;n:type:ShaderForge.SFN_Color,id:5816,x:31970,y:32398,ptovrint:False,ptlb:Outer Rim Color,ptin:_OuterRimColor,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.816075,c3:0.9779412,c4:1;n:type:ShaderForge.SFN_Slider,id:3810,x:31813,y:32607,ptovrint:False,ptlb:Outer Rim Intensity,ptin:_OuterRimIntensity,varname:node_3810,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2.5,max:10;n:type:ShaderForge.SFN_OneMinus,id:4045,x:31719,y:32950,varname:node_4045,prsc:2|IN-1056-OUT;n:type:ShaderForge.SFN_Color,id:6980,x:31719,y:33124,ptovrint:False,ptlb:Inner Rim Color,ptin:_InnerRimColor,varname:_Color_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.05498486,c2:0.09244186,c3:0.8308824,c4:1;n:type:ShaderForge.SFN_Slider,id:5950,x:31798,y:33300,ptovrint:False,ptlb:Inner Rim Intensity,ptin:_InnerRimIntensity,varname:_node_3810_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:6,max:10;n:type:ShaderForge.SFN_Multiply,id:4458,x:32185,y:32988,varname:node_4458,prsc:2|A-2553-OUT,B-6980-RGB,C-5950-OUT,D-6034-OUT;n:type:ShaderForge.SFN_Add,id:5946,x:32402,y:32810,varname:node_5946,prsc:2|A-1148-RGB,B-9603-OUT,C-4458-OUT;n:type:ShaderForge.SFN_RemapRange,id:2553,x:31970,y:32950,varname:node_2553,prsc:2,frmn:0,frmx:1,tomn:0,tomx:10|IN-4045-OUT;n:type:ShaderForge.SFN_Color,id:1148,x:32192,y:32513,ptovrint:False,ptlb:Base Color,ptin:_BaseColor,varname:node_1148,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6102941,c2:0,c3:0.5429513,c4:1;n:type:ShaderForge.SFN_Time,id:3551,x:30528,y:32847,varname:node_3551,prsc:2;n:type:ShaderForge.SFN_Sin,id:1977,x:30986,y:32888,varname:node_1977,prsc:2|IN-2833-OUT;n:type:ShaderForge.SFN_Multiply,id:2833,x:30816,y:32888,varname:node_2833,prsc:2|A-3551-T,B-4610-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4610,x:30528,y:33029,ptovrint:False,ptlb:Rim Flickering Speed,ptin:_RimFlickeringSpeed,varname:node_4610,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.35;n:type:ShaderForge.SFN_Abs,id:7071,x:31161,y:32888,varname:node_7071,prsc:2|IN-1977-OUT;n:type:ShaderForge.SFN_RemapRange,id:6034,x:31338,y:32888,varname:node_6034,prsc:2,frmn:0,frmx:1,tomn:0.5,tomx:1|IN-7071-OUT;proporder:1148-5816-3810-6980-5950-4610;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_LavaLamp_Volume" {
    Properties {
        _BaseColor ("Base Color", Color) = (0.6102941,0,0.5429513,1)
        _OuterRimColor ("Outer Rim Color", Color) = (0,0.816075,0.9779412,1)
        _OuterRimIntensity ("Outer Rim Intensity", Range(0, 10)) = 2.5
        _InnerRimColor ("Inner Rim Color", Color) = (0.05498486,0.09244186,0.8308824,1)
        _InnerRimIntensity ("Inner Rim Intensity", Range(0, 10)) = 6
        _RimFlickeringSpeed ("Rim Flickering Speed", Float ) = 0.35
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _OuterRimColor;
            uniform float _OuterRimIntensity;
            uniform float4 _InnerRimColor;
            uniform float _InnerRimIntensity;
            uniform float4 _BaseColor;
            uniform float _RimFlickeringSpeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_1056 = (1.0-max(0,dot(i.normalDir, viewDirection)));
                float4 node_3551 = _Time + _TimeEditor;
                float node_6034 = (abs(sin((node_3551.g*_RimFlickeringSpeed)))*0.5+0.5);
                float3 emissive = (_BaseColor.rgb+(_OuterRimColor.rgb*_OuterRimIntensity*node_1056*node_6034)+(((1.0 - node_1056)*10.0+0.0)*_InnerRimColor.rgb*_InnerRimIntensity*node_6034));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
