// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-903-OUT;n:type:ShaderForge.SFN_Cubemap,id:9835,x:31937,y:32357,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_9835,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:2a3ec67763a393546a96a22f60815b96,pvfc:0;n:type:ShaderForge.SFN_Color,id:4240,x:31700,y:32846,ptovrint:False,ptlb:Main Color,ptin:_MainColor,varname:node_4240,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:8827,x:32206,y:32515,varname:node_8827,prsc:2|A-9835-RGB,B-8625-OUT,C-3957-RGB;n:type:ShaderForge.SFN_Blend,id:903,x:32437,y:32812,varname:node_903,prsc:2,blmd:5,clmp:True|SRC-8827-OUT,DST-7452-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8625,x:31937,y:32532,ptovrint:False,ptlb:Reflection,ptin:_Reflection,varname:node_8625,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Lerp,id:7452,x:32060,y:32834,varname:node_7452,prsc:2|A-4240-RGB,B-3957-RGB,T-1023-OUT;n:type:ShaderForge.SFN_Fresnel,id:5310,x:31813,y:33060,varname:node_5310,prsc:2|NRM-1530-OUT;n:type:ShaderForge.SFN_NormalVector,id:1530,x:31613,y:33060,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:1023,x:32060,y:33060,varname:node_1023,prsc:2|A-5310-OUT,B-2296-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2296,x:31813,y:33252,ptovrint:False,ptlb:Rim Intensity,ptin:_RimIntensity,varname:node_2296,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.2;n:type:ShaderForge.SFN_Tex2dAsset,id:4765,x:31504,y:32603,ptovrint:False,ptlb:Texture Color,ptin:_TextureColor,varname:node_4765,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bf812b3b358a89f4eae7800a72f79457,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:3957,x:31700,y:32603,varname:node_3957,prsc:2,tex:bf812b3b358a89f4eae7800a72f79457,ntxv:0,isnm:False|TEX-4765-TEX;proporder:4765-9835-4240-8625-2296;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_DyeBottle" {
    Properties {
        _TextureColor ("Texture Color", 2D) = "white" {}
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _MainColor ("Main Color", Color) = (0,0,0,1)
        _Reflection ("Reflection", Float ) = 1
        _RimIntensity ("Rim Intensity", Float ) = 1.2
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform samplerCUBE _Cubemap;
            uniform float4 _MainColor;
            uniform float _Reflection;
            uniform float _RimIntensity;
            uniform sampler2D _TextureColor; uniform float4 _TextureColor_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
////// Lighting:
////// Emissive:
                float4 node_3957 = tex2D(_TextureColor,TRANSFORM_TEX(i.uv0, _TextureColor));
                float3 emissive = saturate(max((texCUBE(_Cubemap,viewReflectDirection).rgb*_Reflection*node_3957.rgb),lerp(_MainColor.rgb,node_3957.rgb,((1.0-max(0,dot(i.normalDir, viewDirection)))*_RimIntensity))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
