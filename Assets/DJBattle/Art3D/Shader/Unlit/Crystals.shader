// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32880,y:32530,varname:node_3138,prsc:2|emission-2020-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32112,y:32476,ptovrint:False,ptlb:Rim Color,ptin:_RimColor,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Time,id:9952,x:31229,y:32806,varname:node_9952,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3537,x:31597,y:32846,varname:node_3537,prsc:2|A-6478-OUT,B-5512-OUT;n:type:ShaderForge.SFN_Sin,id:6478,x:31399,y:32806,varname:node_6478,prsc:2|IN-9952-T;n:type:ShaderForge.SFN_Slider,id:7436,x:32017,y:32884,ptovrint:False,ptlb:Rim Intensity,ptin:_RimIntensity,varname:_node_7436,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.15,max:2;n:type:ShaderForge.SFN_Fresnel,id:1498,x:32112,y:32662,varname:node_1498,prsc:2|NRM-2926-OUT,EXP-5281-OUT;n:type:ShaderForge.SFN_Exp,id:5281,x:31778,y:32810,varname:node_5281,prsc:2,et:0|IN-3537-OUT;n:type:ShaderForge.SFN_Multiply,id:4606,x:32410,y:32534,varname:node_4606,prsc:2|A-7241-RGB,B-1498-OUT,C-7436-OUT;n:type:ShaderForge.SFN_Color,id:3982,x:32179,y:32270,ptovrint:False,ptlb:Base Color,ptin:_BaseColor,varname:node_3982,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1816609,c2:0.4788958,c3:0.7720588,c4:1;n:type:ShaderForge.SFN_Add,id:2020,x:32610,y:32434,varname:node_2020,prsc:2|A-3982-RGB,B-4606-OUT;n:type:ShaderForge.SFN_NormalVector,id:2926,x:31775,y:32562,prsc:2,pt:False;n:type:ShaderForge.SFN_Vector1,id:5512,x:31379,y:32977,varname:node_5512,prsc:2,v1:1.15;proporder:3982-7241-7436;pass:END;sub:END;*/

Shader "Shader Forge/Crystals" {
    Properties {
        _BaseColor ("Base Color", Color) = (0.1816609,0.4788958,0.7720588,1)
        _RimColor ("Rim Color", Color) = (0,1,1,1)
        _RimIntensity ("Rim Intensity", Range(0, 2)) = 1.15
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _RimColor;
            uniform float _RimIntensity;
            uniform float4 _BaseColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_9952 = _Time + _TimeEditor;
                float3 emissive = (_BaseColor.rgb+(_RimColor.rgb*pow(1.0-max(0,dot(i.normalDir, viewDirection)),exp((sin(node_9952.g)*1.15)))*_RimIntensity));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
