// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:34450,y:32754,varname:node_3138,prsc:2|emission-3655-OUT;n:type:ShaderForge.SFN_Tex2d,id:5849,x:31774,y:32466,ptovrint:False,ptlb:Base Color,ptin:_BaseColor,varname:node_5849,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:2668,x:31089,y:33233,ptovrint:False,ptlb:Base Mask,ptin:_BaseMask,varname:node_2668,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5053,x:31039,y:34145,ptovrint:False,ptlb:Glow Mask,ptin:_GlowMask,varname:node_5053,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-6118-OUT;n:type:ShaderForge.SFN_Fresnel,id:5368,x:32993,y:32310,varname:node_5368,prsc:2|NRM-6561-OUT;n:type:ShaderForge.SFN_NormalVector,id:6561,x:32800,y:32300,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:8445,x:33259,y:32370,varname:node_8445,prsc:2|A-8384-OUT,B-5368-OUT;n:type:ShaderForge.SFN_Slider,id:6205,x:32544,y:32532,ptovrint:False,ptlb:Rim Intensity,ptin:_RimIntensity,varname:node_6205,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:5;n:type:ShaderForge.SFN_Multiply,id:6159,x:33773,y:32736,varname:node_6159,prsc:2|A-8445-OUT,B-3879-OUT,C-7448-OUT;n:type:ShaderForge.SFN_Color,id:8591,x:31774,y:32844,ptovrint:False,ptlb:Red Mask Color,ptin:_RedMaskColor,varname:node_8591,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.6744422,c3:0.7352941,c4:1;n:type:ShaderForge.SFN_Multiply,id:9414,x:32039,y:32923,varname:node_9414,prsc:2|A-68-OUT,B-8591-RGB,C-2668-R;n:type:ShaderForge.SFN_Multiply,id:3879,x:33208,y:33096,varname:node_3879,prsc:2|A-5849-RGB,B-9165-OUT,C-5050-OUT,D-6619-OUT;n:type:ShaderForge.SFN_OneMinus,id:4941,x:32039,y:33092,varname:node_4941,prsc:2|IN-2668-R;n:type:ShaderForge.SFN_Add,id:9165,x:32275,y:33017,varname:node_9165,prsc:2|A-9414-OUT,B-4941-OUT;n:type:ShaderForge.SFN_Multiply,id:2517,x:32039,y:33385,varname:node_2517,prsc:2|A-2668-G,B-2638-OUT,C-4468-OUT;n:type:ShaderForge.SFN_OneMinus,id:1548,x:32039,y:33236,varname:node_1548,prsc:2|IN-2668-G;n:type:ShaderForge.SFN_Add,id:5050,x:32274,y:33306,varname:node_5050,prsc:2|A-1548-OUT,B-2517-OUT;n:type:ShaderForge.SFN_Color,id:371,x:31815,y:33453,ptovrint:False,ptlb:Green Mask Color,ptin:_GreenMaskColor,varname:_RedMaskColor_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.9172413,c3:1,c4:1;n:type:ShaderForge.SFN_Slider,id:68,x:31617,y:32721,ptovrint:False,ptlb:Red Mask Intensity,ptin:_RedMaskIntensity,varname:node_68,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2,max:5;n:type:ShaderForge.SFN_Slider,id:4468,x:31658,y:33632,ptovrint:False,ptlb:Green Mask Intensity,ptin:_GreenMaskIntensity,varname:node_4468,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2,max:5;n:type:ShaderForge.SFN_Append,id:6118,x:30834,y:34276,varname:node_6118,prsc:2|A-7045-U,B-9588-OUT;n:type:ShaderForge.SFN_TexCoord,id:7045,x:30362,y:34395,varname:node_7045,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Time,id:3152,x:29990,y:33919,varname:node_3152,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2529,x:30383,y:34081,varname:node_2529,prsc:2|A-3152-T,B-1455-OUT;n:type:ShaderForge.SFN_Add,id:9588,x:30599,y:34197,varname:node_9588,prsc:2|A-2529-OUT,B-7045-V;n:type:ShaderForge.SFN_Slider,id:1455,x:29833,y:34173,ptovrint:False,ptlb:Glow Anim Speed,ptin:_GlowAnimSpeed,varname:node_1455,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.429314,max:1;n:type:ShaderForge.SFN_Multiply,id:5420,x:31532,y:33934,varname:node_5420,prsc:2|A-2668-B,B-1410-OUT;n:type:ShaderForge.SFN_Slider,id:5574,x:30710,y:34668,ptovrint:False,ptlb:Glow Intensity,ptin:_GlowIntensity,varname:node_5574,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2,max:5;n:type:ShaderForge.SFN_OneMinus,id:5352,x:31532,y:33778,varname:node_5352,prsc:2|IN-2668-B;n:type:ShaderForge.SFN_Hue,id:2638,x:31263,y:34174,varname:node_2638,prsc:2|IN-5053-G;n:type:ShaderForge.SFN_Add,id:6619,x:32047,y:33868,varname:node_6619,prsc:2|A-5352-OUT,B-5420-OUT;n:type:ShaderForge.SFN_Add,id:3655,x:33943,y:32820,varname:node_3655,prsc:2|A-6159-OUT,B-3879-OUT;n:type:ShaderForge.SFN_Multiply,id:3269,x:32701,y:32783,varname:node_3269,prsc:2|A-4941-OUT,B-1548-OUT,C-5352-OUT;n:type:ShaderForge.SFN_Color,id:9736,x:32701,y:32634,ptovrint:False,ptlb:Rim Color,ptin:_RimColor,varname:node_9736,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:8194,x:32895,y:32672,varname:node_8194,prsc:2|A-6205-OUT,B-9736-RGB,C-3269-OUT;n:type:ShaderForge.SFN_OneMinus,id:2955,x:33006,y:32810,varname:node_2955,prsc:2|IN-3269-OUT;n:type:ShaderForge.SFN_Add,id:7448,x:33276,y:32720,varname:node_7448,prsc:2|A-8194-OUT,B-2955-OUT;n:type:ShaderForge.SFN_Vector1,id:8384,x:32993,y:32222,varname:node_8384,prsc:2,v1:2;n:type:ShaderForge.SFN_Sin,id:4960,x:31228,y:34349,varname:node_4960,prsc:2|IN-9855-OUT;n:type:ShaderForge.SFN_Tau,id:7210,x:30850,y:34429,varname:node_7210,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:1410,x:31434,y:34349,varname:node_1410,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-4960-OUT;n:type:ShaderForge.SFN_Multiply,id:9855,x:31045,y:34349,varname:node_9855,prsc:2|A-7210-OUT,B-5574-OUT,C-3152-T;proporder:5849-2668-8591-371-68-4468-5053-1455-5574-6205-9736;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_Texture_ColorMask_AnimatedGlow" {
    Properties {
        _BaseColor ("Base Color", 2D) = "white" {}
        _BaseMask ("Base Mask", 2D) = "white" {}
        _RedMaskColor ("Red Mask Color", Color) = (0,0.6744422,0.7352941,1)
        _GreenMaskColor ("Green Mask Color", Color) = (0,0.9172413,1,1)
        _RedMaskIntensity ("Red Mask Intensity", Range(0, 5)) = 2
        _GreenMaskIntensity ("Green Mask Intensity", Range(0, 5)) = 2
        _GlowMask ("Glow Mask", 2D) = "white" {}
        _GlowAnimSpeed ("Glow Anim Speed", Range(0, 1)) = 0.429314
        _GlowIntensity ("Glow Intensity", Range(0, 5)) = 2
        _RimIntensity ("Rim Intensity", Range(0, 5)) = 1
        _RimColor ("Rim Color", Color) = (1,1,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _BaseColor; uniform float4 _BaseColor_ST;
            uniform sampler2D _BaseMask; uniform float4 _BaseMask_ST;
            uniform sampler2D _GlowMask; uniform float4 _GlowMask_ST;
            uniform float _RimIntensity;
            uniform float4 _RedMaskColor;
            uniform float _RedMaskIntensity;
            uniform float _GreenMaskIntensity;
            uniform float _GlowAnimSpeed;
            uniform float _GlowIntensity;
            uniform float4 _RimColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _BaseColor_var = tex2D(_BaseColor,TRANSFORM_TEX(i.uv0, _BaseColor));
                float4 _BaseMask_var = tex2D(_BaseMask,TRANSFORM_TEX(i.uv0, _BaseMask));
                float node_4941 = (1.0 - _BaseMask_var.r);
                float node_1548 = (1.0 - _BaseMask_var.g);
                float4 node_3152 = _Time;
                float2 node_6118 = float2(i.uv0.r,((node_3152.g*_GlowAnimSpeed)+i.uv0.g));
                float4 _GlowMask_var = tex2D(_GlowMask,TRANSFORM_TEX(node_6118, _GlowMask));
                float node_5352 = (1.0 - _BaseMask_var.b);
                float3 node_3879 = (_BaseColor_var.rgb*((_RedMaskIntensity*_RedMaskColor.rgb*_BaseMask_var.r)+node_4941)*(node_1548+(_BaseMask_var.g*saturate(3.0*abs(1.0-2.0*frac(_GlowMask_var.g+float3(0.0,-1.0/3.0,1.0/3.0)))-1)*_GreenMaskIntensity))*(node_5352+(_BaseMask_var.b*(sin((6.28318530718*_GlowIntensity*node_3152.g))*0.5+0.5))));
                float node_3269 = (node_4941*node_1548*node_5352);
                float3 emissive = (((2.0*(1.0-max(0,dot(i.normalDir, viewDirection))))*node_3879*((_RimIntensity*_RimColor.rgb*node_3269)+(1.0 - node_3269)))+node_3879);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
